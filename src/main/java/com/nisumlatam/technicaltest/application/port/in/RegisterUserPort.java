package com.nisumlatam.technicaltest.application.port.in;

public interface RegisterUserPort {

    public UserCommand registerUser(UserCommand userCommand);

}
