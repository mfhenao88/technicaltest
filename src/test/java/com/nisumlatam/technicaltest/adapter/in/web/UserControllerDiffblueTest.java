package com.nisumlatam.technicaltest.adapter.in.web;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.nisumlatam.technicaltest.adapter.in.web.model.PhoneRequest;
import com.nisumlatam.technicaltest.adapter.in.web.model.UserRequest;
import com.nisumlatam.technicaltest.adapter.in.web.model.UserResponse;
import com.nisumlatam.technicaltest.application.port.in.PhoneCommand;
import com.nisumlatam.technicaltest.application.port.in.RegisterUserPort;
import com.nisumlatam.technicaltest.application.port.in.UserCommand;

import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;

class UserControllerDiffblueTest {
    /**
     * Method under test: {@link UserController#registerUser(UserRequest)}
     */
    @Test
    void testRegisterUser() {
        //   Diffblue Cover was unable to create a Spring-specific test for this Spring method.

        // Arrange
        UserCommand userCommand = new UserCommand();
        userCommand.setActive(true);
        Date creationDate = Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        userCommand.setCreationDate(creationDate);
        userCommand.setEmail("jane.doe@example.org");
        UUID idUser = UUID.randomUUID();
        userCommand.setIdUser(idUser);
        Date lastLogin = Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        userCommand.setLastLogin(lastLogin);
        Date modificationDate = Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        userCommand.setModificationDate(modificationDate);
        userCommand.setName("Name");
        userCommand.setPassword("iloveyou");
        userCommand.setPhones(new ArrayList<>());
        userCommand.setToken("ABC123");
        RegisterUserPort registerUserPort = mock(RegisterUserPort.class);
        when(registerUserPort.registerUser(Mockito.<UserCommand>any())).thenReturn(userCommand);
        UserController userController = new UserController(registerUserPort);

        UserRequest userRequest = new UserRequest();
        userRequest.setEmail("jane.doe@example.org");
        userRequest.setName("Name");
        userRequest.setPassword("iloveyou");
        userRequest.setPhones(new ArrayList<>());

        // Act
        ResponseEntity<UserResponse> actualRegisterUserResult = userController.registerUser(userRequest);

        // Assert
        verify(registerUserPort).registerUser(Mockito.<UserCommand>any());
        UserResponse body = actualRegisterUserResult.getBody();
        assertEquals("ABC123", body.getToken());
        assertEquals(200, actualRegisterUserResult.getStatusCodeValue());
        assertTrue(body.getActive());
        assertTrue(actualRegisterUserResult.hasBody());
        assertTrue(actualRegisterUserResult.getHeaders().isEmpty());
        assertSame(creationDate, body.getCreationDate());
        assertSame(lastLogin, body.getLastLogin());
        assertSame(modificationDate, body.getModificationDate());
        assertSame(idUser, body.getIdUser());
    }

    /**
     * Method under test: {@link UserController#registerUser(UserRequest)}
     */
    @Test
    void testRegisterUser2() {
        //   Diffblue Cover was unable to create a Spring-specific test for this Spring method.

        // Arrange
        UserCommand userCommand = mock(UserCommand.class);
        when(userCommand.getActive()).thenReturn(true);
        when(userCommand.getToken()).thenReturn("ABC123");
        when(userCommand.getCreationDate()).thenReturn(null);
        Date fromResult = Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        when(userCommand.getLastLogin()).thenReturn(fromResult);
        Date fromResult2 = Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        when(userCommand.getModificationDate()).thenReturn(fromResult2);
        UUID randomUUIDResult = UUID.randomUUID();
        when(userCommand.getIdUser()).thenReturn(randomUUIDResult);
        doNothing().when(userCommand).setActive(Mockito.<Boolean>any());
        doNothing().when(userCommand).setCreationDate(Mockito.<Date>any());
        doNothing().when(userCommand).setEmail(Mockito.<String>any());
        doNothing().when(userCommand).setIdUser(Mockito.<UUID>any());
        doNothing().when(userCommand).setLastLogin(Mockito.<Date>any());
        doNothing().when(userCommand).setModificationDate(Mockito.<Date>any());
        doNothing().when(userCommand).setName(Mockito.<String>any());
        doNothing().when(userCommand).setPassword(Mockito.<String>any());
        doNothing().when(userCommand).setPhones(Mockito.<List<PhoneCommand>>any());
        doNothing().when(userCommand).setToken(Mockito.<String>any());
        userCommand.setActive(true);
        userCommand.setCreationDate(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        userCommand.setEmail("jane.doe@example.org");
        userCommand.setIdUser(UUID.randomUUID());
        userCommand.setLastLogin(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        userCommand
                .setModificationDate(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        userCommand.setName("Name");
        userCommand.setPassword("iloveyou");
        userCommand.setPhones(new ArrayList<>());
        userCommand.setToken("ABC123");
        RegisterUserPort registerUserPort = mock(RegisterUserPort.class);
        when(registerUserPort.registerUser(Mockito.<UserCommand>any())).thenReturn(userCommand);
        UserController userController = new UserController(registerUserPort);

        UserRequest userRequest = new UserRequest();
        userRequest.setEmail("jane.doe@example.org");
        userRequest.setName("Name");
        userRequest.setPassword("iloveyou");
        userRequest.setPhones(new ArrayList<>());

        // Act
        ResponseEntity<UserResponse> actualRegisterUserResult = userController.registerUser(userRequest);

        // Assert
        verify(registerUserPort).registerUser(Mockito.<UserCommand>any());
        verify(userCommand).getActive();
        verify(userCommand).getCreationDate();
        verify(userCommand).getIdUser();
        verify(userCommand).getLastLogin();
        verify(userCommand).getModificationDate();
        verify(userCommand).getToken();
        verify(userCommand).setActive(Mockito.<Boolean>any());
        verify(userCommand).setCreationDate(Mockito.<Date>any());
        verify(userCommand).setEmail(eq("jane.doe@example.org"));
        verify(userCommand).setIdUser(Mockito.<UUID>any());
        verify(userCommand).setLastLogin(Mockito.<Date>any());
        verify(userCommand).setModificationDate(Mockito.<Date>any());
        verify(userCommand).setName(eq("Name"));
        verify(userCommand).setPassword(eq("iloveyou"));
        verify(userCommand).setPhones(Mockito.<List<PhoneCommand>>any());
        verify(userCommand).setToken(eq("ABC123"));
        UserResponse body = actualRegisterUserResult.getBody();
        assertEquals("ABC123", body.getToken());
        assertEquals(200, actualRegisterUserResult.getStatusCodeValue());
        assertTrue(body.getActive());
        assertTrue(actualRegisterUserResult.hasBody());
        assertTrue(actualRegisterUserResult.getHeaders().isEmpty());
        assertSame(fromResult, body.getLastLogin());
        assertSame(fromResult2, body.getModificationDate());
        assertSame(randomUUIDResult, body.getIdUser());
    }

    /**
     * Method under test: {@link UserController#registerUser(UserRequest)}
     */
    @Test
    void testRegisterUser3() {
        //   Diffblue Cover was unable to create a Spring-specific test for this Spring method.

        // Arrange
        UserCommand userCommand = mock(UserCommand.class);
        when(userCommand.getActive()).thenReturn(true);
        when(userCommand.getToken()).thenReturn("ABC123");
        Date fromResult = Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        when(userCommand.getCreationDate()).thenReturn(fromResult);
        Date fromResult2 = Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        when(userCommand.getLastLogin()).thenReturn(fromResult2);
        Date fromResult3 = Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        when(userCommand.getModificationDate()).thenReturn(fromResult3);
        UUID randomUUIDResult = UUID.randomUUID();
        when(userCommand.getIdUser()).thenReturn(randomUUIDResult);
        doNothing().when(userCommand).setActive(Mockito.<Boolean>any());
        doNothing().when(userCommand).setCreationDate(Mockito.<Date>any());
        doNothing().when(userCommand).setEmail(Mockito.<String>any());
        doNothing().when(userCommand).setIdUser(Mockito.<UUID>any());
        doNothing().when(userCommand).setLastLogin(Mockito.<Date>any());
        doNothing().when(userCommand).setModificationDate(Mockito.<Date>any());
        doNothing().when(userCommand).setName(Mockito.<String>any());
        doNothing().when(userCommand).setPassword(Mockito.<String>any());
        doNothing().when(userCommand).setPhones(Mockito.<List<PhoneCommand>>any());
        doNothing().when(userCommand).setToken(Mockito.<String>any());
        userCommand.setActive(true);
        userCommand.setCreationDate(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        userCommand.setEmail("jane.doe@example.org");
        userCommand.setIdUser(UUID.randomUUID());
        userCommand.setLastLogin(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        userCommand
                .setModificationDate(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        userCommand.setName("Name");
        userCommand.setPassword("iloveyou");
        userCommand.setPhones(new ArrayList<>());
        userCommand.setToken("ABC123");
        RegisterUserPort registerUserPort = mock(RegisterUserPort.class);
        when(registerUserPort.registerUser(Mockito.<UserCommand>any())).thenReturn(userCommand);
        UserController userController = new UserController(registerUserPort);

        PhoneRequest phoneRequest = new PhoneRequest();
        phoneRequest.setCityCode(1);
        phoneRequest.setCountryCode(3);
        phoneRequest.setNumber("42");

        ArrayList<PhoneRequest> phoneRequestList = new ArrayList<>();
        phoneRequestList.add(phoneRequest);
        UserRequest userRequest = mock(UserRequest.class);
        when(userRequest.getEmail()).thenReturn("jane.doe@example.org");
        when(userRequest.getName()).thenReturn("Name");
        when(userRequest.getPassword()).thenReturn("iloveyou");
        when(userRequest.getPhones()).thenReturn(phoneRequestList);
        doNothing().when(userRequest).setEmail(Mockito.<String>any());
        doNothing().when(userRequest).setName(Mockito.<String>any());
        doNothing().when(userRequest).setPassword(Mockito.<String>any());
        doNothing().when(userRequest).setPhones(Mockito.<List<PhoneRequest>>any());
        userRequest.setEmail("jane.doe@example.org");
        userRequest.setName("Name");
        userRequest.setPassword("iloveyou");
        userRequest.setPhones(new ArrayList<>());

        // Act
        ResponseEntity<UserResponse> actualRegisterUserResult = userController.registerUser(userRequest);

        // Assert
        verify(userRequest).getEmail();
        verify(userRequest).getName();
        verify(userRequest).getPassword();
        verify(userRequest).getPhones();
        verify(userRequest).setEmail(eq("jane.doe@example.org"));
        verify(userRequest).setName(eq("Name"));
        verify(userRequest).setPassword(eq("iloveyou"));
        verify(userRequest).setPhones(Mockito.<List<PhoneRequest>>any());
        verify(registerUserPort).registerUser(Mockito.<UserCommand>any());
        verify(userCommand).getActive();
        verify(userCommand).getCreationDate();
        verify(userCommand).getIdUser();
        verify(userCommand).getLastLogin();
        verify(userCommand).getModificationDate();
        verify(userCommand).getToken();
        verify(userCommand).setActive(Mockito.<Boolean>any());
        verify(userCommand).setCreationDate(Mockito.<Date>any());
        verify(userCommand).setEmail(eq("jane.doe@example.org"));
        verify(userCommand).setIdUser(Mockito.<UUID>any());
        verify(userCommand).setLastLogin(Mockito.<Date>any());
        verify(userCommand).setModificationDate(Mockito.<Date>any());
        verify(userCommand).setName(eq("Name"));
        verify(userCommand).setPassword(eq("iloveyou"));
        verify(userCommand).setPhones(Mockito.<List<PhoneCommand>>any());
        verify(userCommand).setToken(eq("ABC123"));
        UserResponse body = actualRegisterUserResult.getBody();
        assertEquals("ABC123", body.getToken());
        assertEquals(200, actualRegisterUserResult.getStatusCodeValue());
        assertTrue(body.getActive());
        assertTrue(actualRegisterUserResult.hasBody());
        assertTrue(actualRegisterUserResult.getHeaders().isEmpty());
        assertSame(fromResult, body.getCreationDate());
        assertSame(fromResult2, body.getLastLogin());
        assertSame(fromResult3, body.getModificationDate());
        assertSame(randomUUIDResult, body.getIdUser());
    }

    /**
     * Method under test: {@link UserController#registerUser(UserRequest)}
     */
    @Test
    void testRegisterUser4() {
        //   Diffblue Cover was unable to create a Spring-specific test for this Spring method.

        // Arrange
        UserCommand userCommand = mock(UserCommand.class);
        when(userCommand.getActive()).thenReturn(true);
        when(userCommand.getToken()).thenReturn("ABC123");
        Date fromResult = Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        when(userCommand.getCreationDate()).thenReturn(fromResult);
        Date fromResult2 = Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        when(userCommand.getLastLogin()).thenReturn(fromResult2);
        Date fromResult3 = Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        when(userCommand.getModificationDate()).thenReturn(fromResult3);
        UUID randomUUIDResult = UUID.randomUUID();
        when(userCommand.getIdUser()).thenReturn(randomUUIDResult);
        doNothing().when(userCommand).setActive(Mockito.<Boolean>any());
        doNothing().when(userCommand).setCreationDate(Mockito.<Date>any());
        doNothing().when(userCommand).setEmail(Mockito.<String>any());
        doNothing().when(userCommand).setIdUser(Mockito.<UUID>any());
        doNothing().when(userCommand).setLastLogin(Mockito.<Date>any());
        doNothing().when(userCommand).setModificationDate(Mockito.<Date>any());
        doNothing().when(userCommand).setName(Mockito.<String>any());
        doNothing().when(userCommand).setPassword(Mockito.<String>any());
        doNothing().when(userCommand).setPhones(Mockito.<List<PhoneCommand>>any());
        doNothing().when(userCommand).setToken(Mockito.<String>any());
        userCommand.setActive(true);
        userCommand.setCreationDate(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        userCommand.setEmail("jane.doe@example.org");
        userCommand.setIdUser(UUID.randomUUID());
        userCommand.setLastLogin(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        userCommand
                .setModificationDate(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        userCommand.setName("Name");
        userCommand.setPassword("iloveyou");
        userCommand.setPhones(new ArrayList<>());
        userCommand.setToken("ABC123");
        RegisterUserPort registerUserPort = mock(RegisterUserPort.class);
        when(registerUserPort.registerUser(Mockito.<UserCommand>any())).thenReturn(userCommand);
        UserController userController = new UserController(registerUserPort);

        PhoneRequest phoneRequest = new PhoneRequest();
        phoneRequest.setCityCode(1);
        phoneRequest.setCountryCode(3);
        phoneRequest.setNumber("42");

        PhoneRequest phoneRequest2 = new PhoneRequest();
        phoneRequest2.setCityCode(0);
        phoneRequest2.setCountryCode(1);
        phoneRequest2.setNumber("Number");

        ArrayList<PhoneRequest> phoneRequestList = new ArrayList<>();
        phoneRequestList.add(phoneRequest2);
        phoneRequestList.add(phoneRequest);
        UserRequest userRequest = mock(UserRequest.class);
        when(userRequest.getEmail()).thenReturn("jane.doe@example.org");
        when(userRequest.getName()).thenReturn("Name");
        when(userRequest.getPassword()).thenReturn("iloveyou");
        when(userRequest.getPhones()).thenReturn(phoneRequestList);
        doNothing().when(userRequest).setEmail(Mockito.<String>any());
        doNothing().when(userRequest).setName(Mockito.<String>any());
        doNothing().when(userRequest).setPassword(Mockito.<String>any());
        doNothing().when(userRequest).setPhones(Mockito.<List<PhoneRequest>>any());
        userRequest.setEmail("jane.doe@example.org");
        userRequest.setName("Name");
        userRequest.setPassword("iloveyou");
        userRequest.setPhones(new ArrayList<>());

        // Act
        ResponseEntity<UserResponse> actualRegisterUserResult = userController.registerUser(userRequest);

        // Assert
        verify(userRequest).getEmail();
        verify(userRequest).getName();
        verify(userRequest).getPassword();
        verify(userRequest).getPhones();
        verify(userRequest).setEmail(eq("jane.doe@example.org"));
        verify(userRequest).setName(eq("Name"));
        verify(userRequest).setPassword(eq("iloveyou"));
        verify(userRequest).setPhones(Mockito.<List<PhoneRequest>>any());
        verify(registerUserPort).registerUser(Mockito.<UserCommand>any());
        verify(userCommand).getActive();
        verify(userCommand).getCreationDate();
        verify(userCommand).getIdUser();
        verify(userCommand).getLastLogin();
        verify(userCommand).getModificationDate();
        verify(userCommand).getToken();
        verify(userCommand).setActive(Mockito.<Boolean>any());
        verify(userCommand).setCreationDate(Mockito.<Date>any());
        verify(userCommand).setEmail(eq("jane.doe@example.org"));
        verify(userCommand).setIdUser(Mockito.<UUID>any());
        verify(userCommand).setLastLogin(Mockito.<Date>any());
        verify(userCommand).setModificationDate(Mockito.<Date>any());
        verify(userCommand).setName(eq("Name"));
        verify(userCommand).setPassword(eq("iloveyou"));
        verify(userCommand).setPhones(Mockito.<List<PhoneCommand>>any());
        verify(userCommand).setToken(eq("ABC123"));
        UserResponse body = actualRegisterUserResult.getBody();
        assertEquals("ABC123", body.getToken());
        assertEquals(200, actualRegisterUserResult.getStatusCodeValue());
        assertTrue(body.getActive());
        assertTrue(actualRegisterUserResult.hasBody());
        assertTrue(actualRegisterUserResult.getHeaders().isEmpty());
        assertSame(fromResult, body.getCreationDate());
        assertSame(fromResult2, body.getLastLogin());
        assertSame(fromResult3, body.getModificationDate());
        assertSame(randomUUIDResult, body.getIdUser());
    }

    /**
     * Method under test: {@link UserController#registerUser(UserRequest)}
     */
    @Test
    void testRegisterUser5() {
        //   Diffblue Cover was unable to create a Spring-specific test for this Spring method.

        // Arrange
        UserCommand userCommand = mock(UserCommand.class);
        when(userCommand.getActive()).thenReturn(true);
        when(userCommand.getToken()).thenReturn("ABC123");
        Date fromResult = Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        when(userCommand.getCreationDate()).thenReturn(fromResult);
        Date fromResult2 = Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        when(userCommand.getLastLogin()).thenReturn(fromResult2);
        Date fromResult3 = Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        when(userCommand.getModificationDate()).thenReturn(fromResult3);
        UUID randomUUIDResult = UUID.randomUUID();
        when(userCommand.getIdUser()).thenReturn(randomUUIDResult);
        doNothing().when(userCommand).setActive(Mockito.<Boolean>any());
        doNothing().when(userCommand).setCreationDate(Mockito.<Date>any());
        doNothing().when(userCommand).setEmail(Mockito.<String>any());
        doNothing().when(userCommand).setIdUser(Mockito.<UUID>any());
        doNothing().when(userCommand).setLastLogin(Mockito.<Date>any());
        doNothing().when(userCommand).setModificationDate(Mockito.<Date>any());
        doNothing().when(userCommand).setName(Mockito.<String>any());
        doNothing().when(userCommand).setPassword(Mockito.<String>any());
        doNothing().when(userCommand).setPhones(Mockito.<List<PhoneCommand>>any());
        doNothing().when(userCommand).setToken(Mockito.<String>any());
        userCommand.setActive(true);
        userCommand.setCreationDate(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        userCommand.setEmail("jane.doe@example.org");
        userCommand.setIdUser(UUID.randomUUID());
        userCommand.setLastLogin(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        userCommand
                .setModificationDate(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        userCommand.setName("Name");
        userCommand.setPassword("iloveyou");
        userCommand.setPhones(new ArrayList<>());
        userCommand.setToken("ABC123");
        RegisterUserPort registerUserPort = mock(RegisterUserPort.class);
        when(registerUserPort.registerUser(Mockito.<UserCommand>any())).thenReturn(userCommand);
        UserController userController = new UserController(registerUserPort);

        PhoneRequest phoneRequest = new PhoneRequest();
        phoneRequest.setCityCode(1);
        phoneRequest.setCountryCode(3);
        phoneRequest.setNumber("42");

        ArrayList<PhoneRequest> phoneRequestList = new ArrayList<>();
        phoneRequestList.add(phoneRequest);
        UserRequest userRequest = mock(UserRequest.class);
        when(userRequest.getEmail()).thenReturn("42");
        when(userRequest.getName()).thenReturn("Name");
        when(userRequest.getPassword()).thenReturn("iloveyou");
        when(userRequest.getPhones()).thenReturn(phoneRequestList);
        doNothing().when(userRequest).setEmail(Mockito.<String>any());
        doNothing().when(userRequest).setName(Mockito.<String>any());
        doNothing().when(userRequest).setPassword(Mockito.<String>any());
        doNothing().when(userRequest).setPhones(Mockito.<List<PhoneRequest>>any());
        userRequest.setEmail("jane.doe@example.org");
        userRequest.setName("Name");
        userRequest.setPassword("iloveyou");
        userRequest.setPhones(new ArrayList<>());

        // Act
        ResponseEntity<UserResponse> actualRegisterUserResult = userController.registerUser(userRequest);

        // Assert
        verify(userRequest).getEmail();
        verify(userRequest).getName();
        verify(userRequest).getPassword();
        verify(userRequest).getPhones();
        verify(userRequest).setEmail(eq("jane.doe@example.org"));
        verify(userRequest).setName(eq("Name"));
        verify(userRequest).setPassword(eq("iloveyou"));
        verify(userRequest).setPhones(Mockito.<List<PhoneRequest>>any());
        verify(registerUserPort).registerUser(Mockito.<UserCommand>any());
        verify(userCommand).getActive();
        verify(userCommand).getCreationDate();
        verify(userCommand).getIdUser();
        verify(userCommand).getLastLogin();
        verify(userCommand).getModificationDate();
        verify(userCommand).getToken();
        verify(userCommand).setActive(Mockito.<Boolean>any());
        verify(userCommand).setCreationDate(Mockito.<Date>any());
        verify(userCommand).setEmail(eq("jane.doe@example.org"));
        verify(userCommand).setIdUser(Mockito.<UUID>any());
        verify(userCommand).setLastLogin(Mockito.<Date>any());
        verify(userCommand).setModificationDate(Mockito.<Date>any());
        verify(userCommand).setName(eq("Name"));
        verify(userCommand).setPassword(eq("iloveyou"));
        verify(userCommand).setPhones(Mockito.<List<PhoneCommand>>any());
        verify(userCommand).setToken(eq("ABC123"));
        UserResponse body = actualRegisterUserResult.getBody();
        assertEquals("ABC123", body.getToken());
        assertEquals(200, actualRegisterUserResult.getStatusCodeValue());
        assertTrue(body.getActive());
        assertTrue(actualRegisterUserResult.hasBody());
        assertTrue(actualRegisterUserResult.getHeaders().isEmpty());
        assertSame(fromResult, body.getCreationDate());
        assertSame(fromResult2, body.getLastLogin());
        assertSame(fromResult3, body.getModificationDate());
        assertSame(randomUUIDResult, body.getIdUser());
    }

    /**
     * Method under test: {@link UserController#registerUser(UserRequest)}
     */
    @Test
    void testRegisterUser6() {
        //   Diffblue Cover was unable to create a Spring-specific test for this Spring method.

        // Arrange
        UserCommand userCommand = mock(UserCommand.class);
        when(userCommand.getActive()).thenReturn(false);
        when(userCommand.getToken()).thenReturn("ABC123");
        Date fromResult = Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        when(userCommand.getCreationDate()).thenReturn(fromResult);
        Date fromResult2 = Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        when(userCommand.getLastLogin()).thenReturn(fromResult2);
        Date fromResult3 = Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        when(userCommand.getModificationDate()).thenReturn(fromResult3);
        UUID randomUUIDResult = UUID.randomUUID();
        when(userCommand.getIdUser()).thenReturn(randomUUIDResult);
        doNothing().when(userCommand).setActive(Mockito.<Boolean>any());
        doNothing().when(userCommand).setCreationDate(Mockito.<Date>any());
        doNothing().when(userCommand).setEmail(Mockito.<String>any());
        doNothing().when(userCommand).setIdUser(Mockito.<UUID>any());
        doNothing().when(userCommand).setLastLogin(Mockito.<Date>any());
        doNothing().when(userCommand).setModificationDate(Mockito.<Date>any());
        doNothing().when(userCommand).setName(Mockito.<String>any());
        doNothing().when(userCommand).setPassword(Mockito.<String>any());
        doNothing().when(userCommand).setPhones(Mockito.<List<PhoneCommand>>any());
        doNothing().when(userCommand).setToken(Mockito.<String>any());
        userCommand.setActive(true);
        userCommand.setCreationDate(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        userCommand.setEmail("jane.doe@example.org");
        userCommand.setIdUser(UUID.randomUUID());
        userCommand.setLastLogin(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        userCommand
                .setModificationDate(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        userCommand.setName("Name");
        userCommand.setPassword("iloveyou");
        userCommand.setPhones(new ArrayList<>());
        userCommand.setToken("ABC123");
        RegisterUserPort registerUserPort = mock(RegisterUserPort.class);
        when(registerUserPort.registerUser(Mockito.<UserCommand>any())).thenReturn(userCommand);
        UserController userController = new UserController(registerUserPort);

        PhoneRequest phoneRequest = new PhoneRequest();
        phoneRequest.setCityCode(1);
        phoneRequest.setCountryCode(3);
        phoneRequest.setNumber("42");

        PhoneRequest phoneRequest2 = new PhoneRequest();
        phoneRequest2.setCityCode(0);
        phoneRequest2.setCountryCode(1);
        phoneRequest2.setNumber("Number");

        ArrayList<PhoneRequest> phoneRequestList = new ArrayList<>();
        phoneRequestList.add(phoneRequest2);
        phoneRequestList.add(phoneRequest);
        UserRequest userRequest = mock(UserRequest.class);
        when(userRequest.getEmail()).thenReturn("jane.doe@example.org");
        when(userRequest.getName()).thenReturn("Name");
        when(userRequest.getPassword()).thenReturn("iloveyou");
        when(userRequest.getPhones()).thenReturn(phoneRequestList);
        doNothing().when(userRequest).setEmail(Mockito.<String>any());
        doNothing().when(userRequest).setName(Mockito.<String>any());
        doNothing().when(userRequest).setPassword(Mockito.<String>any());
        doNothing().when(userRequest).setPhones(Mockito.<List<PhoneRequest>>any());
        userRequest.setEmail("jane.doe@example.org");
        userRequest.setName("Name");
        userRequest.setPassword("iloveyou");
        userRequest.setPhones(new ArrayList<>());

        // Act
        ResponseEntity<UserResponse> actualRegisterUserResult = userController.registerUser(userRequest);

        // Assert
        verify(userRequest).getEmail();
        verify(userRequest).getName();
        verify(userRequest).getPassword();
        verify(userRequest).getPhones();
        verify(userRequest).setEmail(eq("jane.doe@example.org"));
        verify(userRequest).setName(eq("Name"));
        verify(userRequest).setPassword(eq("iloveyou"));
        verify(userRequest).setPhones(Mockito.<List<PhoneRequest>>any());
        verify(registerUserPort).registerUser(Mockito.<UserCommand>any());
        verify(userCommand).getActive();
        verify(userCommand).getCreationDate();
        verify(userCommand).getIdUser();
        verify(userCommand).getLastLogin();
        verify(userCommand).getModificationDate();
        verify(userCommand).getToken();
        verify(userCommand).setActive(Mockito.<Boolean>any());
        verify(userCommand).setCreationDate(Mockito.<Date>any());
        verify(userCommand).setEmail(eq("jane.doe@example.org"));
        verify(userCommand).setIdUser(Mockito.<UUID>any());
        verify(userCommand).setLastLogin(Mockito.<Date>any());
        verify(userCommand).setModificationDate(Mockito.<Date>any());
        verify(userCommand).setName(eq("Name"));
        verify(userCommand).setPassword(eq("iloveyou"));
        verify(userCommand).setPhones(Mockito.<List<PhoneCommand>>any());
        verify(userCommand).setToken(eq("ABC123"));
        UserResponse body = actualRegisterUserResult.getBody();
        assertEquals("ABC123", body.getToken());
        assertEquals(200, actualRegisterUserResult.getStatusCodeValue());
        assertFalse(body.getActive());
        assertTrue(actualRegisterUserResult.hasBody());
        assertTrue(actualRegisterUserResult.getHeaders().isEmpty());
        assertSame(fromResult, body.getCreationDate());
        assertSame(fromResult2, body.getLastLogin());
        assertSame(fromResult3, body.getModificationDate());
        assertSame(randomUUIDResult, body.getIdUser());
    }
}
