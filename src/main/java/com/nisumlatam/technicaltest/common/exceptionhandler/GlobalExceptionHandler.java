package com.nisumlatam.technicaltest.common.exceptionhandler;


import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorApi> handleValidationErrors(MethodArgumentNotValidException ex) {
        List<String> errors = ex.getBindingResult().getFieldErrors().stream().map(FieldError::getDefaultMessage).toList();
        return new ResponseEntity<>(getError(String.join(", ", errors)), new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<ErrorApi> handleGeneralExceptions(Exception ex) {
        return new ResponseEntity<>(getError(ex.getMessage()), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(RuntimeException.class)
    public final ResponseEntity<ErrorApi> handleRuntimeExceptions(RuntimeException ex) {
        return new ResponseEntity<>(getError(ex.getMessage()), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(ValidationException.class)
    public final ResponseEntity<ErrorApi> handleRuntimeExceptions(ValidationException ex) {
        return new ResponseEntity<>(getError(ex.getMessage()), new HttpHeaders(), HttpStatus.CONFLICT);
    }

    private ErrorApi getError(String error) {
        ErrorApi errorApi = new ErrorApi();
        errorApi.setMessage(error);
        return errorApi;
    }
}
