package com.nisumlatam.technicaltest.adapter.out.persistence.user;

import com.nisumlatam.technicaltest.application.port.out.InsertUserPort;
import com.nisumlatam.technicaltest.application.port.out.LoadUserPort;
import com.nisumlatam.technicaltest.domain.User;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class UserPersistenceAdapter implements InsertUserPort, LoadUserPort {

    @Autowired
    private UserRepository userRepository;

    private ModelMapper modelMapper;

    public UserPersistenceAdapter() {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setSkipNullEnabled(true);
        this.modelMapper = modelMapper;
    }

    @Override
    public Optional<User> load(String email) {
        UserEntity userEntity = userRepository.findByEmail(email);
        return Optional.ofNullable(userEntity != null ? modelMapper.map(userEntity, User.class) : null);
    }

    @Override
    public Optional<User> insertUser(User user) {
        UserEntity userEntity = modelMapper.map(user, UserEntity.class);
        UUID uuidUser = UUID.randomUUID();
        userEntity.setIdUser(uuidUser);
        userEntity.getPhones().forEach(phoneEntity -> {
            phoneEntity.setIdPhone(UUID.randomUUID());
            phoneEntity.setUser(userEntity);
        });
        return Optional.of(modelMapper.map(userRepository.save(userEntity), User.class));
    }
}
