package com.nisumlatam.technicaltest.adapter.in.web.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;
import java.util.UUID;

@ApiModel(description = "Response DTO for User")
public class UserResponse {
    @ApiModelProperty(value = "User ID")
    private UUID idUser;

    @ApiModelProperty(value = "Creation date")
    private Date creationDate;

    @ApiModelProperty(value = "Modification date")
    private Date modificationDate;

    @ApiModelProperty(value = "Last login date")
    private Date lastLogin;

    @ApiModelProperty(value = "User token")
    private String token;

    @ApiModelProperty(value = "User active status")
    private Boolean isActive;

    public UUID getIdUser() {
        return idUser;
    }

    public void setIdUser(UUID idUser) {
        this.idUser = idUser;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    public Date getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }
}
