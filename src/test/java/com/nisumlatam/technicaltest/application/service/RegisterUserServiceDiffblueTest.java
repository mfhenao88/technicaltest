package com.nisumlatam.technicaltest.application.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.nisumlatam.technicaltest.application.port.in.PhoneCommand;
import com.nisumlatam.technicaltest.application.port.in.UserCommand;
import com.nisumlatam.technicaltest.application.port.out.InsertUserPort;
import com.nisumlatam.technicaltest.application.port.out.LoadUserPort;
import com.nisumlatam.technicaltest.domain.Phone;
import com.nisumlatam.technicaltest.domain.User;

import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class RegisterUserServiceDiffblueTest {
    /**
     * Method under test:
     * {@link RegisterUserService#RegisterUserService(LoadUserPort, InsertUserPort)}
     */
    @Test
    void testNewRegisterUserService() {
        // Arrange
        LoadUserPort loadUserPort = mock(LoadUserPort.class);
        Optional<User> emptyResult = Optional.empty();
        when(loadUserPort.load(Mockito.<String>any())).thenReturn(emptyResult);

        User user = new User();
        user.setActive(true);
        Date creationDate = Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        user.setCreationDate(creationDate);
        user.setEmail("jane.doe@example.org");
        UUID id = UUID.randomUUID();
        user.setId(id);
        Date lastLogin = Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        user.setLastLogin(lastLogin);
        Date modificationDate = Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        user.setModificationDate(modificationDate);
        user.setName("Name");
        user.setPassword("iloveyou");
        ArrayList<Phone> phones = new ArrayList<>();
        user.setPhones(phones);
        user.setToken("ABC123");
        Optional<User> ofResult = Optional.of(user);
        InsertUserPort insertUserPort = mock(InsertUserPort.class);
        when(insertUserPort.insertUser(Mockito.<User>any())).thenReturn(ofResult);

        // Act
        RegisterUserService actualRegisterUserService = new RegisterUserService(loadUserPort, insertUserPort);
        UserCommand userCommand = new UserCommand();
        userCommand.setActive(true);
        userCommand.setCreationDate(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        userCommand.setEmail("jane.doe@example.org");
        userCommand.setIdUser(UUID.randomUUID());
        userCommand.setLastLogin(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        userCommand
                .setModificationDate(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        userCommand.setName("Name");
        userCommand.setPassword("iloveyou");
        userCommand.setPhones(new ArrayList<>());
        userCommand.setToken("ABC123");
        UserCommand actualRegisterUserResult = actualRegisterUserService.registerUser(userCommand);

        // Assert
        verify(insertUserPort).insertUser(Mockito.<User>any());
        verify(loadUserPort).load(eq("jane.doe@example.org"));
        assertEquals("ABC123", actualRegisterUserResult.getToken());
        assertEquals("Name", actualRegisterUserResult.getName());
        assertEquals("iloveyou", actualRegisterUserResult.getPassword());
        assertEquals("jane.doe@example.org", actualRegisterUserResult.getEmail());
        assertTrue(actualRegisterUserResult.getActive());
        assertEquals(phones, actualRegisterUserResult.getPhones());
        assertSame(creationDate, actualRegisterUserResult.getCreationDate());
        assertSame(lastLogin, actualRegisterUserResult.getLastLogin());
        assertSame(modificationDate, actualRegisterUserResult.getModificationDate());
        assertSame(id, actualRegisterUserResult.getIdUser());
    }

    /**
     * Method under test:
     * {@link RegisterUserService#RegisterUserService(LoadUserPort, InsertUserPort)}
     */
    @Test
    void testNewRegisterUserService2() {
        // Arrange
        LoadUserPort loadUserPort = mock(LoadUserPort.class);
        Optional<User> emptyResult = Optional.empty();
        when(loadUserPort.load(Mockito.<String>any())).thenReturn(emptyResult);

        User user = new User();
        user.setActive(true);
        user.setCreationDate(null);
        user.setEmail("jane.doe@example.org");
        UUID id = UUID.randomUUID();
        user.setId(id);
        Date lastLogin = Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        user.setLastLogin(lastLogin);
        Date modificationDate = Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        user.setModificationDate(modificationDate);
        user.setName("Name");
        user.setPassword("iloveyou");
        ArrayList<Phone> phones = new ArrayList<>();
        user.setPhones(phones);
        user.setToken("ABC123");
        Optional<User> ofResult = Optional.of(user);
        InsertUserPort insertUserPort = mock(InsertUserPort.class);
        when(insertUserPort.insertUser(Mockito.<User>any())).thenReturn(ofResult);

        // Act
        RegisterUserService actualRegisterUserService = new RegisterUserService(loadUserPort, insertUserPort);
        UserCommand userCommand = new UserCommand();
        userCommand.setActive(true);
        userCommand.setCreationDate(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        userCommand.setEmail("jane.doe@example.org");
        userCommand.setIdUser(UUID.randomUUID());
        userCommand.setLastLogin(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        userCommand
                .setModificationDate(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        userCommand.setName("Name");
        userCommand.setPassword("iloveyou");
        userCommand.setPhones(new ArrayList<>());
        userCommand.setToken("ABC123");
        UserCommand actualRegisterUserResult = actualRegisterUserService.registerUser(userCommand);

        // Assert
        verify(insertUserPort).insertUser(Mockito.<User>any());
        verify(loadUserPort).load(eq("jane.doe@example.org"));
        assertEquals("ABC123", actualRegisterUserResult.getToken());
        assertEquals("Name", actualRegisterUserResult.getName());
        assertEquals("iloveyou", actualRegisterUserResult.getPassword());
        assertEquals("jane.doe@example.org", actualRegisterUserResult.getEmail());
        assertTrue(actualRegisterUserResult.getActive());
        assertEquals(phones, actualRegisterUserResult.getPhones());
        assertSame(lastLogin, actualRegisterUserResult.getLastLogin());
        assertSame(modificationDate, actualRegisterUserResult.getModificationDate());
        assertSame(id, actualRegisterUserResult.getIdUser());
    }

    /**
     * Method under test:
     * {@link RegisterUserService#RegisterUserService(LoadUserPort, InsertUserPort)}
     */
    @Test
    void testNewRegisterUserService3() {
        // Arrange
        LoadUserPort loadUserPort = mock(LoadUserPort.class);
        Optional<User> emptyResult = Optional.empty();
        when(loadUserPort.load(Mockito.<String>any())).thenReturn(emptyResult);

        User user = new User();
        user.setActive(true);
        Date creationDate = Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        user.setCreationDate(creationDate);
        user.setEmail("jane.doe@example.org");
        UUID id = UUID.randomUUID();
        user.setId(id);
        Date lastLogin = Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        user.setLastLogin(lastLogin);
        Date modificationDate = Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        user.setModificationDate(modificationDate);
        user.setName("Name");
        user.setPassword("iloveyou");
        ArrayList<Phone> phones = new ArrayList<>();
        user.setPhones(phones);
        user.setToken("ABC123");
        Optional<User> ofResult = Optional.of(user);
        InsertUserPort insertUserPort = mock(InsertUserPort.class);
        when(insertUserPort.insertUser(Mockito.<User>any())).thenReturn(ofResult);

        // Act
        RegisterUserService actualRegisterUserService = new RegisterUserService(loadUserPort, insertUserPort);
        PhoneCommand phoneCommand = new PhoneCommand();
        phoneCommand.setCityCode(1);
        phoneCommand.setCountryCode(3);
        phoneCommand.setNumber("42");
        ArrayList<PhoneCommand> phones2 = new ArrayList<>();
        phones2.add(phoneCommand);
        UserCommand userCommand = new UserCommand();
        userCommand.setActive(true);
        userCommand.setCreationDate(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        userCommand.setEmail("jane.doe@example.org");
        userCommand.setIdUser(UUID.randomUUID());
        userCommand.setLastLogin(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        userCommand
                .setModificationDate(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        userCommand.setName("Name");
        userCommand.setPassword("iloveyou");
        userCommand.setPhones(phones2);
        userCommand.setToken("ABC123");
        UserCommand actualRegisterUserResult = actualRegisterUserService.registerUser(userCommand);

        // Assert
        verify(insertUserPort).insertUser(Mockito.<User>any());
        verify(loadUserPort).load(eq("jane.doe@example.org"));
        assertEquals("ABC123", actualRegisterUserResult.getToken());
        assertEquals("Name", actualRegisterUserResult.getName());
        assertEquals("iloveyou", actualRegisterUserResult.getPassword());
        assertEquals("jane.doe@example.org", actualRegisterUserResult.getEmail());
        assertTrue(actualRegisterUserResult.getActive());
        assertEquals(phones, actualRegisterUserResult.getPhones());
        assertSame(creationDate, actualRegisterUserResult.getCreationDate());
        assertSame(lastLogin, actualRegisterUserResult.getLastLogin());
        assertSame(modificationDate, actualRegisterUserResult.getModificationDate());
        assertSame(id, actualRegisterUserResult.getIdUser());
    }

    /**
     * Method under test: {@link RegisterUserService#registerUser(UserCommand)}
     */
    @Test
    void testRegisterUser() {
        // Arrange
        User user = new User();
        user.setActive(true);
        user.setCreationDate(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        user.setEmail("jane.doe@example.org");
        user.setId(UUID.randomUUID());
        user.setLastLogin(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        user.setModificationDate(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        user.setName("Name");
        user.setPassword("iloveyou");
        user.setPhones(new ArrayList<>());
        user.setToken("ABC123");
        Optional<User> ofResult = Optional.of(user);
        LoadUserPort loadUserPort = mock(LoadUserPort.class);
        when(loadUserPort.load(Mockito.<String>any())).thenReturn(ofResult);
        RegisterUserService registerUserService = new RegisterUserService(loadUserPort, mock(InsertUserPort.class));

        UserCommand userCommand = new UserCommand();
        userCommand.setActive(true);
        userCommand.setCreationDate(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        userCommand.setEmail("jane.doe@example.org");
        userCommand.setIdUser(UUID.randomUUID());
        userCommand.setLastLogin(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        userCommand
                .setModificationDate(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        userCommand.setName("Name");
        userCommand.setPassword("iloveyou");
        userCommand.setPhones(new ArrayList<>());
        userCommand.setToken("ABC123");

        // Act and Assert
        assertThrows(RuntimeException.class, () -> registerUserService.registerUser(userCommand));
        verify(loadUserPort).load(eq("jane.doe@example.org"));
    }

    /**
     * Method under test: {@link RegisterUserService#registerUser(UserCommand)}
     */
    @Test
    void testRegisterUser2() {
        // Arrange
        LoadUserPort loadUserPort = mock(LoadUserPort.class);
        Optional<User> emptyResult = Optional.empty();
        when(loadUserPort.load(Mockito.<String>any())).thenReturn(emptyResult);

        User user = new User();
        user.setActive(true);
        Date creationDate = Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        user.setCreationDate(creationDate);
        user.setEmail("jane.doe@example.org");
        UUID id = UUID.randomUUID();
        user.setId(id);
        Date lastLogin = Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        user.setLastLogin(lastLogin);
        Date modificationDate = Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        user.setModificationDate(modificationDate);
        user.setName("Name");
        user.setPassword("iloveyou");
        ArrayList<Phone> phones = new ArrayList<>();
        user.setPhones(phones);
        user.setToken("ABC123");
        Optional<User> ofResult = Optional.of(user);
        InsertUserPort insertUserPort = mock(InsertUserPort.class);
        when(insertUserPort.insertUser(Mockito.<User>any())).thenReturn(ofResult);
        RegisterUserService registerUserService = new RegisterUserService(loadUserPort, insertUserPort);

        UserCommand userCommand = new UserCommand();
        userCommand.setActive(true);
        userCommand.setCreationDate(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        userCommand.setEmail("jane.doe@example.org");
        userCommand.setIdUser(UUID.randomUUID());
        userCommand.setLastLogin(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        userCommand
                .setModificationDate(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        userCommand.setName("Name");
        userCommand.setPassword("iloveyou");
        userCommand.setPhones(new ArrayList<>());
        userCommand.setToken("ABC123");

        // Act
        UserCommand actualRegisterUserResult = registerUserService.registerUser(userCommand);

        // Assert
        verify(insertUserPort).insertUser(Mockito.<User>any());
        verify(loadUserPort).load(eq("jane.doe@example.org"));
        assertEquals("ABC123", actualRegisterUserResult.getToken());
        assertEquals("Name", actualRegisterUserResult.getName());
        assertEquals("iloveyou", actualRegisterUserResult.getPassword());
        assertEquals("jane.doe@example.org", actualRegisterUserResult.getEmail());
        assertTrue(actualRegisterUserResult.getActive());
        assertEquals(phones, actualRegisterUserResult.getPhones());
        assertSame(creationDate, actualRegisterUserResult.getCreationDate());
        assertSame(lastLogin, actualRegisterUserResult.getLastLogin());
        assertSame(modificationDate, actualRegisterUserResult.getModificationDate());
        assertSame(id, actualRegisterUserResult.getIdUser());
    }

    /**
     * Method under test: {@link RegisterUserService#registerUser(UserCommand)}
     */
    @Test
    void testRegisterUser3() {
        // Arrange
        LoadUserPort loadUserPort = mock(LoadUserPort.class);
        Optional<User> emptyResult = Optional.empty();
        when(loadUserPort.load(Mockito.<String>any())).thenReturn(emptyResult);

        User user = new User();
        user.setActive(true);
        user.setCreationDate(null);
        user.setEmail("jane.doe@example.org");
        UUID id = UUID.randomUUID();
        user.setId(id);
        Date lastLogin = Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        user.setLastLogin(lastLogin);
        Date modificationDate = Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        user.setModificationDate(modificationDate);
        user.setName("Name");
        user.setPassword("iloveyou");
        ArrayList<Phone> phones = new ArrayList<>();
        user.setPhones(phones);
        user.setToken("ABC123");
        Optional<User> ofResult = Optional.of(user);
        InsertUserPort insertUserPort = mock(InsertUserPort.class);
        when(insertUserPort.insertUser(Mockito.<User>any())).thenReturn(ofResult);
        RegisterUserService registerUserService = new RegisterUserService(loadUserPort, insertUserPort);

        UserCommand userCommand = new UserCommand();
        userCommand.setActive(true);
        userCommand.setCreationDate(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        userCommand.setEmail("jane.doe@example.org");
        userCommand.setIdUser(UUID.randomUUID());
        userCommand.setLastLogin(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        userCommand
                .setModificationDate(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        userCommand.setName("Name");
        userCommand.setPassword("iloveyou");
        userCommand.setPhones(new ArrayList<>());
        userCommand.setToken("ABC123");

        // Act
        UserCommand actualRegisterUserResult = registerUserService.registerUser(userCommand);

        // Assert
        verify(insertUserPort).insertUser(Mockito.<User>any());
        verify(loadUserPort).load(eq("jane.doe@example.org"));
        assertEquals("ABC123", actualRegisterUserResult.getToken());
        assertEquals("Name", actualRegisterUserResult.getName());
        assertEquals("iloveyou", actualRegisterUserResult.getPassword());
        assertEquals("jane.doe@example.org", actualRegisterUserResult.getEmail());
        assertTrue(actualRegisterUserResult.getActive());
        assertEquals(phones, actualRegisterUserResult.getPhones());
        assertSame(lastLogin, actualRegisterUserResult.getLastLogin());
        assertSame(modificationDate, actualRegisterUserResult.getModificationDate());
        assertSame(id, actualRegisterUserResult.getIdUser());
    }

    /**
     * Method under test: {@link RegisterUserService#registerUser(UserCommand)}
     */
    @Test
    void testRegisterUser4() {
        // Arrange
        LoadUserPort loadUserPort = mock(LoadUserPort.class);
        Optional<User> emptyResult = Optional.empty();
        when(loadUserPort.load(Mockito.<String>any())).thenReturn(emptyResult);
        InsertUserPort insertUserPort = mock(InsertUserPort.class);
        Optional<User> emptyResult2 = Optional.empty();
        when(insertUserPort.insertUser(Mockito.<User>any())).thenReturn(emptyResult2);
        RegisterUserService registerUserService = new RegisterUserService(loadUserPort, insertUserPort);

        UserCommand userCommand = new UserCommand();
        userCommand.setActive(true);
        userCommand.setCreationDate(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        userCommand.setEmail("jane.doe@example.org");
        userCommand.setIdUser(UUID.randomUUID());
        userCommand.setLastLogin(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        userCommand
                .setModificationDate(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        userCommand.setName("Name");
        userCommand.setPassword("iloveyou");
        userCommand.setPhones(new ArrayList<>());
        userCommand.setToken("ABC123");

        // Act and Assert
        assertThrows(RuntimeException.class, () -> registerUserService.registerUser(userCommand));
        verify(insertUserPort).insertUser(Mockito.<User>any());
        verify(loadUserPort).load(eq("jane.doe@example.org"));
    }

    /**
     * Method under test: {@link RegisterUserService#registerUser(UserCommand)}
     */
    @Test
    void testRegisterUser5() {
        // Arrange
        LoadUserPort loadUserPort = mock(LoadUserPort.class);
        Optional<User> emptyResult = Optional.empty();
        when(loadUserPort.load(Mockito.<String>any())).thenReturn(emptyResult);

        User user = new User();
        user.setActive(true);
        Date creationDate = Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        user.setCreationDate(creationDate);
        user.setEmail("jane.doe@example.org");
        UUID id = UUID.randomUUID();
        user.setId(id);
        Date lastLogin = Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        user.setLastLogin(lastLogin);
        Date modificationDate = Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        user.setModificationDate(modificationDate);
        user.setName("Name");
        user.setPassword("iloveyou");
        ArrayList<Phone> phones = new ArrayList<>();
        user.setPhones(phones);
        user.setToken("ABC123");
        Optional<User> ofResult = Optional.of(user);
        InsertUserPort insertUserPort = mock(InsertUserPort.class);
        when(insertUserPort.insertUser(Mockito.<User>any())).thenReturn(ofResult);
        RegisterUserService registerUserService = new RegisterUserService(loadUserPort, insertUserPort);

        PhoneCommand phoneCommand = new PhoneCommand();
        phoneCommand.setCityCode(1);
        phoneCommand.setCountryCode(3);
        phoneCommand.setNumber("42");

        ArrayList<PhoneCommand> phones2 = new ArrayList<>();
        phones2.add(phoneCommand);

        UserCommand userCommand = new UserCommand();
        userCommand.setActive(true);
        userCommand.setCreationDate(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        userCommand.setEmail("jane.doe@example.org");
        userCommand.setIdUser(UUID.randomUUID());
        userCommand.setLastLogin(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        userCommand
                .setModificationDate(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        userCommand.setName("Name");
        userCommand.setPassword("iloveyou");
        userCommand.setPhones(phones2);
        userCommand.setToken("ABC123");

        // Act
        UserCommand actualRegisterUserResult = registerUserService.registerUser(userCommand);

        // Assert
        verify(insertUserPort).insertUser(Mockito.<User>any());
        verify(loadUserPort).load(eq("jane.doe@example.org"));
        assertEquals("ABC123", actualRegisterUserResult.getToken());
        assertEquals("Name", actualRegisterUserResult.getName());
        assertEquals("iloveyou", actualRegisterUserResult.getPassword());
        assertEquals("jane.doe@example.org", actualRegisterUserResult.getEmail());
        assertTrue(actualRegisterUserResult.getActive());
        assertEquals(phones, actualRegisterUserResult.getPhones());
        assertSame(creationDate, actualRegisterUserResult.getCreationDate());
        assertSame(lastLogin, actualRegisterUserResult.getLastLogin());
        assertSame(modificationDate, actualRegisterUserResult.getModificationDate());
        assertSame(id, actualRegisterUserResult.getIdUser());
    }

    /**
     * Method under test: {@link RegisterUserService#registerUser(UserCommand)}
     */
    @Test
    void testRegisterUser6() {
        // Arrange
        LoadUserPort loadUserPort = mock(LoadUserPort.class);
        Optional<User> emptyResult = Optional.empty();
        when(loadUserPort.load(Mockito.<String>any())).thenReturn(emptyResult);

        User user = new User();
        user.setActive(true);
        Date creationDate = Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        user.setCreationDate(creationDate);
        user.setEmail("jane.doe@example.org");
        UUID id = UUID.randomUUID();
        user.setId(id);
        Date lastLogin = Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        user.setLastLogin(lastLogin);
        Date modificationDate = Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        user.setModificationDate(modificationDate);
        user.setName("Name");
        user.setPassword("iloveyou");
        ArrayList<Phone> phones = new ArrayList<>();
        user.setPhones(phones);
        user.setToken("ABC123");
        Optional<User> ofResult = Optional.of(user);
        InsertUserPort insertUserPort = mock(InsertUserPort.class);
        when(insertUserPort.insertUser(Mockito.<User>any())).thenReturn(ofResult);
        RegisterUserService registerUserService = new RegisterUserService(loadUserPort, insertUserPort);

        PhoneCommand phoneCommand = new PhoneCommand();
        phoneCommand.setCityCode(1);
        phoneCommand.setCountryCode(3);
        phoneCommand.setNumber("42");

        PhoneCommand phoneCommand2 = new PhoneCommand();
        phoneCommand2.setCityCode(0);
        phoneCommand2.setCountryCode(1);
        phoneCommand2.setNumber("Number");

        ArrayList<PhoneCommand> phones2 = new ArrayList<>();
        phones2.add(phoneCommand2);
        phones2.add(phoneCommand);

        UserCommand userCommand = new UserCommand();
        userCommand.setActive(true);
        userCommand.setCreationDate(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        userCommand.setEmail("jane.doe@example.org");
        userCommand.setIdUser(UUID.randomUUID());
        userCommand.setLastLogin(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        userCommand
                .setModificationDate(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        userCommand.setName("Name");
        userCommand.setPassword("iloveyou");
        userCommand.setPhones(phones2);
        userCommand.setToken("ABC123");

        // Act
        UserCommand actualRegisterUserResult = registerUserService.registerUser(userCommand);

        // Assert
        verify(insertUserPort).insertUser(Mockito.<User>any());
        verify(loadUserPort).load(eq("jane.doe@example.org"));
        assertEquals("ABC123", actualRegisterUserResult.getToken());
        assertEquals("Name", actualRegisterUserResult.getName());
        assertEquals("iloveyou", actualRegisterUserResult.getPassword());
        assertEquals("jane.doe@example.org", actualRegisterUserResult.getEmail());
        assertTrue(actualRegisterUserResult.getActive());
        assertEquals(phones, actualRegisterUserResult.getPhones());
        assertSame(creationDate, actualRegisterUserResult.getCreationDate());
        assertSame(lastLogin, actualRegisterUserResult.getLastLogin());
        assertSame(modificationDate, actualRegisterUserResult.getModificationDate());
        assertSame(id, actualRegisterUserResult.getIdUser());
    }

    /**
     * Method under test: {@link RegisterUserService#registerUser(UserCommand)}
     */
    @Test
    void testRegisterUser7() {
        // Arrange
        LoadUserPort loadUserPort = mock(LoadUserPort.class);
        Optional<User> emptyResult = Optional.empty();
        when(loadUserPort.load(Mockito.<String>any())).thenReturn(emptyResult);
        InsertUserPort insertUserPort = mock(InsertUserPort.class);
        when(insertUserPort.insertUser(Mockito.<User>any())).thenThrow(new RuntimeException("Error executing user insert"));
        RegisterUserService registerUserService = new RegisterUserService(loadUserPort, insertUserPort);

        UserCommand userCommand = new UserCommand();
        userCommand.setActive(true);
        userCommand.setCreationDate(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        userCommand.setEmail("jane.doe@example.org");
        userCommand.setIdUser(UUID.randomUUID());
        userCommand.setLastLogin(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        userCommand
                .setModificationDate(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        userCommand.setName("Name");
        userCommand.setPassword("iloveyou");
        userCommand.setPhones(new ArrayList<>());
        userCommand.setToken("ABC123");

        // Act and Assert
        assertThrows(RuntimeException.class, () -> registerUserService.registerUser(userCommand));
        verify(insertUserPort).insertUser(Mockito.<User>any());
        verify(loadUserPort).load(eq("jane.doe@example.org"));
    }
}
