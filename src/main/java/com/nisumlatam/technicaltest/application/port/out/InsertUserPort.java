package com.nisumlatam.technicaltest.application.port.out;

import com.nisumlatam.technicaltest.domain.User;

import java.util.Optional;

public interface InsertUserPort {
    public Optional<User> insertUser(User user);
}
