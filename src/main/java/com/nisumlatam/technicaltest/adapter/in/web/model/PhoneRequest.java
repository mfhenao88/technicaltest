package com.nisumlatam.technicaltest.adapter.in.web.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotBlank;


public class PhoneRequest {
    @NotBlank(message = "Number cannot be blank")
    @JsonProperty("number")
    private String number;

    @NotBlank(message = "City code cannot be blank")
    @JsonProperty("citycode")
    private int cityCode;

    @NotBlank(message = "Country code cannot be blank")
    @JsonProperty("countrycode")
    private int countryCode;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getCityCode() {
        return cityCode;
    }

    public void setCityCode(int cityCode) {
        this.cityCode = cityCode;
    }

    public int getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(int countryCode) {
        this.countryCode = countryCode;
    }
}
