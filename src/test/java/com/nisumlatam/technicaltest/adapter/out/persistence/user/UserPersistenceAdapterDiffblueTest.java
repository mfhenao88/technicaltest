package com.nisumlatam.technicaltest.adapter.out.persistence.user;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.nisumlatam.technicaltest.adapter.out.persistence.phone.PhoneEntity;
import com.nisumlatam.technicaltest.domain.Phone;
import com.nisumlatam.technicaltest.domain.User;

import java.sql.Date;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.aot.DisabledInAotMode;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {UserPersistenceAdapter.class})
@ExtendWith(SpringExtension.class)
@DisabledInAotMode
class UserPersistenceAdapterDiffblueTest {
    @Autowired
    private UserPersistenceAdapter userPersistenceAdapter;

    @MockBean
    private UserRepository userRepository;

    /**
     * Method under test: {@link UserPersistenceAdapter#load(String)}
     */
    @Test
    void testLoad() {
        // Arrange
        UserEntity userEntity = new UserEntity();
        userEntity.setActive(true);
        userEntity.setCreationDate(mock(Date.class));
        userEntity.setEmail("jane.doe@example.org");
        UUID idUser = UUID.randomUUID();
        userEntity.setIdUser(idUser);
        userEntity.setLastLogin(mock(Date.class));
        userEntity.setModificationDate(mock(Date.class));
        userEntity.setName("Name");
        userEntity.setPassword("iloveyou");
        ArrayList<PhoneEntity> phones = new ArrayList<>();
        userEntity.setPhones(phones);
        userEntity.setToken("ABC123");
        when(userRepository.findByEmail(Mockito.<String>any())).thenReturn(userEntity);

        // Act
        Optional<User> actualLoadResult = userPersistenceAdapter.load("jane.doe@example.org");

        // Assert
        verify(userRepository).findByEmail(eq("jane.doe@example.org"));
        User getResult = actualLoadResult.get();
        assertEquals("ABC123", getResult.getToken());
        assertEquals("Name", getResult.getName());
        assertEquals("iloveyou", getResult.getPassword());
        assertEquals("jane.doe@example.org", getResult.getEmail());
        assertTrue(getResult.getActive());
        assertTrue(actualLoadResult.isPresent());
        assertEquals(phones, getResult.getPhones());
        assertSame(idUser, getResult.getId());
    }

    /**
     * Method under test: {@link UserPersistenceAdapter#load(String)}
     */
    @Test
    void testLoad2() {
        // Arrange
        UserEntity user = new UserEntity();
        user.setActive(true);
        user.setCreationDate(mock(Date.class));
        user.setEmail("jane.doe@example.org");
        user.setIdUser(UUID.randomUUID());
        user.setLastLogin(mock(Date.class));
        user.setModificationDate(mock(Date.class));
        user.setName("Name");
        user.setPassword("iloveyou");
        user.setPhones(new ArrayList<>());
        user.setToken("ABC123");

        PhoneEntity phoneEntity = new PhoneEntity();
        phoneEntity.setCityCode("Oxford");
        phoneEntity.setCountryCode("GB");
        UUID idPhone = UUID.randomUUID();
        phoneEntity.setIdPhone(idPhone);
        phoneEntity.setNumber("42");
        phoneEntity.setUser(user);

        ArrayList<PhoneEntity> phoneEntityList = new ArrayList<>();
        phoneEntityList.add(phoneEntity);
        UserEntity userEntity = mock(UserEntity.class);
        when(userEntity.getActive()).thenReturn(true);
        when(userEntity.getEmail()).thenReturn("jane.doe@example.org");
        when(userEntity.getName()).thenReturn("Name");
        when(userEntity.getPassword()).thenReturn("iloveyou");
        when(userEntity.getToken()).thenReturn("ABC123");
        when(userEntity.getCreationDate()).thenReturn(mock(Date.class));
        when(userEntity.getLastLogin()).thenReturn(mock(Date.class));
        when(userEntity.getModificationDate()).thenReturn(mock(Date.class));
        when(userEntity.getPhones()).thenReturn(phoneEntityList);
        UUID randomUUIDResult = UUID.randomUUID();
        when(userEntity.getIdUser()).thenReturn(randomUUIDResult);
        doNothing().when(userEntity).setActive(Mockito.<Boolean>any());
        doNothing().when(userEntity).setCreationDate(Mockito.<Date>any());
        doNothing().when(userEntity).setEmail(Mockito.<String>any());
        doNothing().when(userEntity).setIdUser(Mockito.<UUID>any());
        doNothing().when(userEntity).setLastLogin(Mockito.<Date>any());
        doNothing().when(userEntity).setModificationDate(Mockito.<Date>any());
        doNothing().when(userEntity).setName(Mockito.<String>any());
        doNothing().when(userEntity).setPassword(Mockito.<String>any());
        doNothing().when(userEntity).setPhones(Mockito.<List<PhoneEntity>>any());
        doNothing().when(userEntity).setToken(Mockito.<String>any());
        userEntity.setActive(true);
        userEntity.setCreationDate(mock(Date.class));
        userEntity.setEmail("jane.doe@example.org");
        userEntity.setIdUser(UUID.randomUUID());
        userEntity.setLastLogin(mock(Date.class));
        userEntity.setModificationDate(mock(Date.class));
        userEntity.setName("Name");
        userEntity.setPassword("iloveyou");
        userEntity.setPhones(new ArrayList<>());
        userEntity.setToken("ABC123");
        when(userRepository.findByEmail(Mockito.<String>any())).thenReturn(userEntity);

        // Act
        Optional<User> actualLoadResult = userPersistenceAdapter.load("jane.doe@example.org");

        // Assert
        verify(userEntity).getActive();
        verify(userEntity).getCreationDate();
        verify(userEntity).getEmail();
        verify(userEntity).getIdUser();
        verify(userEntity).getLastLogin();
        verify(userEntity).getModificationDate();
        verify(userEntity).getName();
        verify(userEntity).getPassword();
        verify(userEntity).getPhones();
        verify(userEntity).getToken();
        verify(userEntity).setActive(Mockito.<Boolean>any());
        verify(userEntity).setCreationDate(Mockito.<Date>any());
        verify(userEntity).setEmail(eq("jane.doe@example.org"));
        verify(userEntity).setIdUser(Mockito.<UUID>any());
        verify(userEntity).setLastLogin(Mockito.<Date>any());
        verify(userEntity).setModificationDate(Mockito.<Date>any());
        verify(userEntity).setName(eq("Name"));
        verify(userEntity).setPassword(eq("iloveyou"));
        verify(userEntity).setPhones(Mockito.<List<PhoneEntity>>any());
        verify(userEntity).setToken(eq("ABC123"));
        verify(userRepository).findByEmail(eq("jane.doe@example.org"));
        User getResult = actualLoadResult.get();
        List<Phone> phones = getResult.getPhones();
        assertEquals(1, phones.size());
        Phone getResult2 = phones.get(0);
        assertEquals("42", getResult2.getNumber());
        assertEquals("ABC123", getResult.getToken());
        assertEquals("GB", getResult2.getCountryCode());
        assertEquals("Name", getResult.getName());
        assertEquals("Oxford", getResult2.getCityCode());
        assertEquals("iloveyou", getResult.getPassword());
        assertEquals("jane.doe@example.org", getResult.getEmail());
        assertTrue(getResult.getActive());
        assertTrue(actualLoadResult.isPresent());
        assertSame(user, getResult2.getUser());
        assertSame(idPhone, getResult2.getId());
        assertSame(randomUUIDResult, getResult.getId());
    }

    /**
     * Method under test: {@link UserPersistenceAdapter#load(String)}
     */
    @Test
    void testLoad3() {
        // Arrange
        UserEntity userEntity = mock(UserEntity.class);
        when(userEntity.getActive()).thenReturn(true);
        when(userEntity.getEmail()).thenReturn("jane.doe@example.org");
        when(userEntity.getName()).thenReturn("Name");
        when(userEntity.getPassword()).thenReturn("iloveyou");
        when(userEntity.getToken()).thenReturn("ABC123");
        when(userEntity.getCreationDate()).thenReturn(mock(Date.class));
        when(userEntity.getLastLogin()).thenReturn(mock(Date.class));
        when(userEntity.getModificationDate()).thenReturn(mock(Date.class));
        ArrayList<PhoneEntity> phoneEntityList = new ArrayList<>();
        when(userEntity.getPhones()).thenReturn(phoneEntityList);
        when(userEntity.getIdUser()).thenReturn(null);
        doNothing().when(userEntity).setActive(Mockito.<Boolean>any());
        doNothing().when(userEntity).setCreationDate(Mockito.<Date>any());
        doNothing().when(userEntity).setEmail(Mockito.<String>any());
        doNothing().when(userEntity).setIdUser(Mockito.<UUID>any());
        doNothing().when(userEntity).setLastLogin(Mockito.<Date>any());
        doNothing().when(userEntity).setModificationDate(Mockito.<Date>any());
        doNothing().when(userEntity).setName(Mockito.<String>any());
        doNothing().when(userEntity).setPassword(Mockito.<String>any());
        doNothing().when(userEntity).setPhones(Mockito.<List<PhoneEntity>>any());
        doNothing().when(userEntity).setToken(Mockito.<String>any());
        userEntity.setActive(true);
        userEntity.setCreationDate(mock(Date.class));
        userEntity.setEmail("jane.doe@example.org");
        userEntity.setIdUser(UUID.randomUUID());
        userEntity.setLastLogin(mock(Date.class));
        userEntity.setModificationDate(mock(Date.class));
        userEntity.setName("Name");
        userEntity.setPassword("iloveyou");
        userEntity.setPhones(new ArrayList<>());
        userEntity.setToken("ABC123");
        when(userRepository.findByEmail(Mockito.<String>any())).thenReturn(userEntity);

        // Act
        Optional<User> actualLoadResult = userPersistenceAdapter.load("jane.doe@example.org");

        // Assert
        verify(userEntity).getActive();
        verify(userEntity).getCreationDate();
        verify(userEntity).getEmail();
        verify(userEntity).getIdUser();
        verify(userEntity).getLastLogin();
        verify(userEntity).getModificationDate();
        verify(userEntity).getName();
        verify(userEntity).getPassword();
        verify(userEntity).getPhones();
        verify(userEntity).getToken();
        verify(userEntity).setActive(Mockito.<Boolean>any());
        verify(userEntity).setCreationDate(Mockito.<Date>any());
        verify(userEntity).setEmail(eq("jane.doe@example.org"));
        verify(userEntity).setIdUser(Mockito.<UUID>any());
        verify(userEntity).setLastLogin(Mockito.<Date>any());
        verify(userEntity).setModificationDate(Mockito.<Date>any());
        verify(userEntity).setName(eq("Name"));
        verify(userEntity).setPassword(eq("iloveyou"));
        verify(userEntity).setPhones(Mockito.<List<PhoneEntity>>any());
        verify(userEntity).setToken(eq("ABC123"));
        verify(userRepository).findByEmail(eq("jane.doe@example.org"));
        User getResult = actualLoadResult.get();
        assertEquals("ABC123", getResult.getToken());
        assertEquals("Name", getResult.getName());
        assertEquals("iloveyou", getResult.getPassword());
        assertEquals("jane.doe@example.org", getResult.getEmail());
        assertTrue(getResult.getActive());
        assertTrue(actualLoadResult.isPresent());
        assertEquals(phoneEntityList, getResult.getPhones());
    }

    /**
     * Method under test: {@link UserPersistenceAdapter#load(String)}
     */
    @Test
    void testLoad4() {
        // Arrange
        UserEntity user = new UserEntity();
        user.setActive(true);
        user.setCreationDate(mock(Date.class));
        user.setEmail("jane.doe@example.org");
        user.setIdUser(UUID.randomUUID());
        user.setLastLogin(mock(Date.class));
        user.setModificationDate(mock(Date.class));
        user.setName("Name");
        user.setPassword("iloveyou");
        user.setPhones(new ArrayList<>());
        user.setToken("ABC123");

        PhoneEntity phoneEntity = new PhoneEntity();
        phoneEntity.setCityCode("Oxford");
        phoneEntity.setCountryCode("GB");
        UUID idPhone = UUID.randomUUID();
        phoneEntity.setIdPhone(idPhone);
        phoneEntity.setNumber("42");
        phoneEntity.setUser(user);

        ArrayList<PhoneEntity> phoneEntityList = new ArrayList<>();
        phoneEntityList.add(phoneEntity);
        UserEntity userEntity = mock(UserEntity.class);
        when(userEntity.getActive()).thenReturn(true);
        when(userEntity.getEmail()).thenReturn("42");
        when(userEntity.getName()).thenReturn("Name");
        when(userEntity.getPassword()).thenReturn("iloveyou");
        when(userEntity.getToken()).thenReturn("ABC123");
        when(userEntity.getCreationDate()).thenReturn(mock(Date.class));
        when(userEntity.getLastLogin()).thenReturn(mock(Date.class));
        when(userEntity.getModificationDate()).thenReturn(mock(Date.class));
        when(userEntity.getPhones()).thenReturn(phoneEntityList);
        UUID randomUUIDResult = UUID.randomUUID();
        when(userEntity.getIdUser()).thenReturn(randomUUIDResult);
        doNothing().when(userEntity).setActive(Mockito.<Boolean>any());
        doNothing().when(userEntity).setCreationDate(Mockito.<Date>any());
        doNothing().when(userEntity).setEmail(Mockito.<String>any());
        doNothing().when(userEntity).setIdUser(Mockito.<UUID>any());
        doNothing().when(userEntity).setLastLogin(Mockito.<Date>any());
        doNothing().when(userEntity).setModificationDate(Mockito.<Date>any());
        doNothing().when(userEntity).setName(Mockito.<String>any());
        doNothing().when(userEntity).setPassword(Mockito.<String>any());
        doNothing().when(userEntity).setPhones(Mockito.<List<PhoneEntity>>any());
        doNothing().when(userEntity).setToken(Mockito.<String>any());
        userEntity.setActive(true);
        userEntity.setCreationDate(mock(Date.class));
        userEntity.setEmail("jane.doe@example.org");
        userEntity.setIdUser(UUID.randomUUID());
        userEntity.setLastLogin(mock(Date.class));
        userEntity.setModificationDate(mock(Date.class));
        userEntity.setName("Name");
        userEntity.setPassword("iloveyou");
        userEntity.setPhones(new ArrayList<>());
        userEntity.setToken("ABC123");
        when(userRepository.findByEmail(Mockito.<String>any())).thenReturn(userEntity);

        // Act
        Optional<User> actualLoadResult = userPersistenceAdapter.load("jane.doe@example.org");

        // Assert
        verify(userEntity).getActive();
        verify(userEntity).getCreationDate();
        verify(userEntity).getEmail();
        verify(userEntity).getIdUser();
        verify(userEntity).getLastLogin();
        verify(userEntity).getModificationDate();
        verify(userEntity).getName();
        verify(userEntity).getPassword();
        verify(userEntity).getPhones();
        verify(userEntity).getToken();
        verify(userEntity).setActive(Mockito.<Boolean>any());
        verify(userEntity).setCreationDate(Mockito.<Date>any());
        verify(userEntity).setEmail(eq("jane.doe@example.org"));
        verify(userEntity).setIdUser(Mockito.<UUID>any());
        verify(userEntity).setLastLogin(Mockito.<Date>any());
        verify(userEntity).setModificationDate(Mockito.<Date>any());
        verify(userEntity).setName(eq("Name"));
        verify(userEntity).setPassword(eq("iloveyou"));
        verify(userEntity).setPhones(Mockito.<List<PhoneEntity>>any());
        verify(userEntity).setToken(eq("ABC123"));
        verify(userRepository).findByEmail(eq("jane.doe@example.org"));
        User getResult = actualLoadResult.get();
        List<Phone> phones = getResult.getPhones();
        assertEquals(1, phones.size());
        Phone getResult2 = phones.get(0);
        assertEquals("42", getResult2.getNumber());
        assertEquals("42", getResult.getEmail());
        assertEquals("ABC123", getResult.getToken());
        assertEquals("GB", getResult2.getCountryCode());
        assertEquals("Name", getResult.getName());
        assertEquals("Oxford", getResult2.getCityCode());
        assertEquals("iloveyou", getResult.getPassword());
        assertTrue(getResult.getActive());
        assertTrue(actualLoadResult.isPresent());
        assertSame(user, getResult2.getUser());
        assertSame(idPhone, getResult2.getId());
        assertSame(randomUUIDResult, getResult.getId());
    }

    /**
     * Method under test: {@link UserPersistenceAdapter#insertUser(User)}
     */
    @Test
    void testInsertUser() {
        // Arrange
        UserEntity userEntity = new UserEntity();
        userEntity.setActive(true);
        userEntity.setCreationDate(mock(java.sql.Date.class));
        userEntity.setEmail("jane.doe@example.org");
        UUID idUser = UUID.randomUUID();
        userEntity.setIdUser(idUser);
        userEntity.setLastLogin(mock(java.sql.Date.class));
        userEntity.setModificationDate(mock(java.sql.Date.class));
        userEntity.setName("Name");
        userEntity.setPassword("iloveyou");
        ArrayList<PhoneEntity> phones = new ArrayList<>();
        userEntity.setPhones(phones);
        userEntity.setToken("ABC123");
        when(userRepository.save(Mockito.<UserEntity>any())).thenReturn(userEntity);

        User user = new User();
        user.setActive(true);
        user.setCreationDate(
                java.util.Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        user.setEmail("jane.doe@example.org");
        user.setId(UUID.randomUUID());
        user.setLastLogin(java.util.Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        user.setModificationDate(
                java.util.Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        user.setName("Name");
        user.setPassword("iloveyou");
        user.setPhones(new ArrayList<>());
        user.setToken("ABC123");

        // Act
        Optional<User> actualInsertUserResult = userPersistenceAdapter.insertUser(user);

        // Assert
        verify(userRepository).save(Mockito.<UserEntity>any());
        User getResult = actualInsertUserResult.get();
        assertEquals("ABC123", getResult.getToken());
        assertEquals("Name", getResult.getName());
        assertEquals("iloveyou", getResult.getPassword());
        assertEquals("jane.doe@example.org", getResult.getEmail());
        assertTrue(getResult.getActive());
        assertTrue(actualInsertUserResult.isPresent());
        assertEquals(phones, getResult.getPhones());
        assertSame(idUser, getResult.getId());
    }

    /**
     * Method under test: {@link UserPersistenceAdapter#insertUser(User)}
     */
    @Test
    void testInsertUser2() {
        // Arrange
        UserEntity user = new UserEntity();
        user.setActive(true);
        user.setCreationDate(mock(java.sql.Date.class));
        user.setEmail("jane.doe@example.org");
        user.setIdUser(UUID.randomUUID());
        user.setLastLogin(mock(java.sql.Date.class));
        user.setModificationDate(mock(java.sql.Date.class));
        user.setName("Name");
        user.setPassword("iloveyou");
        user.setPhones(new ArrayList<>());
        user.setToken("ABC123");

        PhoneEntity phoneEntity = new PhoneEntity();
        phoneEntity.setCityCode("Oxford");
        phoneEntity.setCountryCode("GB");
        UUID idPhone = UUID.randomUUID();
        phoneEntity.setIdPhone(idPhone);
        phoneEntity.setNumber("42");
        phoneEntity.setUser(user);

        ArrayList<PhoneEntity> phoneEntityList = new ArrayList<>();
        phoneEntityList.add(phoneEntity);
        UserEntity userEntity = mock(UserEntity.class);
        when(userEntity.getActive()).thenReturn(true);
        when(userEntity.getEmail()).thenReturn("jane.doe@example.org");
        when(userEntity.getName()).thenReturn("Name");
        when(userEntity.getPassword()).thenReturn("iloveyou");
        when(userEntity.getToken()).thenReturn("ABC123");
        when(userEntity.getCreationDate()).thenReturn(mock(java.sql.Date.class));
        when(userEntity.getLastLogin()).thenReturn(mock(java.sql.Date.class));
        when(userEntity.getModificationDate()).thenReturn(mock(java.sql.Date.class));
        when(userEntity.getPhones()).thenReturn(phoneEntityList);
        UUID randomUUIDResult = UUID.randomUUID();
        when(userEntity.getIdUser()).thenReturn(randomUUIDResult);
        doNothing().when(userEntity).setActive(Mockito.<Boolean>any());
        doNothing().when(userEntity).setCreationDate(Mockito.<java.sql.Date>any());
        doNothing().when(userEntity).setEmail(Mockito.<String>any());
        doNothing().when(userEntity).setIdUser(Mockito.<UUID>any());
        doNothing().when(userEntity).setLastLogin(Mockito.<java.sql.Date>any());
        doNothing().when(userEntity).setModificationDate(Mockito.<java.sql.Date>any());
        doNothing().when(userEntity).setName(Mockito.<String>any());
        doNothing().when(userEntity).setPassword(Mockito.<String>any());
        doNothing().when(userEntity).setPhones(Mockito.<List<PhoneEntity>>any());
        doNothing().when(userEntity).setToken(Mockito.<String>any());
        userEntity.setActive(true);
        userEntity.setCreationDate(mock(java.sql.Date.class));
        userEntity.setEmail("jane.doe@example.org");
        userEntity.setIdUser(UUID.randomUUID());
        userEntity.setLastLogin(mock(java.sql.Date.class));
        userEntity.setModificationDate(mock(java.sql.Date.class));
        userEntity.setName("Name");
        userEntity.setPassword("iloveyou");
        userEntity.setPhones(new ArrayList<>());
        userEntity.setToken("ABC123");
        when(userRepository.save(Mockito.<UserEntity>any())).thenReturn(userEntity);

        User user2 = new User();
        user2.setActive(true);
        user2.setCreationDate(
                java.util.Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        user2.setEmail("jane.doe@example.org");
        user2.setId(UUID.randomUUID());
        user2.setLastLogin(java.util.Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        user2.setModificationDate(
                java.util.Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        user2.setName("Name");
        user2.setPassword("iloveyou");
        user2.setPhones(new ArrayList<>());
        user2.setToken("ABC123");

        // Act
        Optional<User> actualInsertUserResult = userPersistenceAdapter.insertUser(user2);

        // Assert
        verify(userEntity).getActive();
        verify(userEntity).getCreationDate();
        verify(userEntity).getEmail();
        verify(userEntity).getIdUser();
        verify(userEntity).getLastLogin();
        verify(userEntity).getModificationDate();
        verify(userEntity).getName();
        verify(userEntity).getPassword();
        verify(userEntity).getPhones();
        verify(userEntity).getToken();
        verify(userEntity).setActive(Mockito.<Boolean>any());
        verify(userEntity).setCreationDate(Mockito.<java.sql.Date>any());
        verify(userEntity).setEmail(eq("jane.doe@example.org"));
        verify(userEntity).setIdUser(Mockito.<UUID>any());
        verify(userEntity).setLastLogin(Mockito.<java.sql.Date>any());
        verify(userEntity).setModificationDate(Mockito.<java.sql.Date>any());
        verify(userEntity).setName(eq("Name"));
        verify(userEntity).setPassword(eq("iloveyou"));
        verify(userEntity).setPhones(Mockito.<List<PhoneEntity>>any());
        verify(userEntity).setToken(eq("ABC123"));
        verify(userRepository).save(Mockito.<UserEntity>any());
        User getResult = actualInsertUserResult.get();
        List<Phone> phones = getResult.getPhones();
        assertEquals(1, phones.size());
        Phone getResult2 = phones.get(0);
        assertEquals("42", getResult2.getNumber());
        assertEquals("ABC123", getResult.getToken());
        assertEquals("GB", getResult2.getCountryCode());
        assertEquals("Name", getResult.getName());
        assertEquals("Oxford", getResult2.getCityCode());
        assertEquals("iloveyou", getResult.getPassword());
        assertEquals("jane.doe@example.org", getResult.getEmail());
        assertTrue(getResult.getActive());
        assertTrue(actualInsertUserResult.isPresent());
        assertSame(user, getResult2.getUser());
        assertSame(idPhone, getResult2.getId());
        assertSame(randomUUIDResult, getResult.getId());
    }

    /**
     * Method under test: {@link UserPersistenceAdapter#insertUser(User)}
     */
    @Test
    void testInsertUser3() {
        // Arrange
        UserEntity userEntity = mock(UserEntity.class);
        when(userEntity.getActive()).thenReturn(true);
        when(userEntity.getEmail()).thenReturn("jane.doe@example.org");
        when(userEntity.getName()).thenReturn("Name");
        when(userEntity.getPassword()).thenReturn("iloveyou");
        when(userEntity.getToken()).thenReturn("ABC123");
        when(userEntity.getCreationDate()).thenReturn(mock(java.sql.Date.class));
        when(userEntity.getLastLogin()).thenReturn(mock(java.sql.Date.class));
        when(userEntity.getModificationDate()).thenReturn(mock(java.sql.Date.class));
        ArrayList<PhoneEntity> phoneEntityList = new ArrayList<>();
        when(userEntity.getPhones()).thenReturn(phoneEntityList);
        when(userEntity.getIdUser()).thenReturn(null);
        doNothing().when(userEntity).setActive(Mockito.<Boolean>any());
        doNothing().when(userEntity).setCreationDate(Mockito.<java.sql.Date>any());
        doNothing().when(userEntity).setEmail(Mockito.<String>any());
        doNothing().when(userEntity).setIdUser(Mockito.<UUID>any());
        doNothing().when(userEntity).setLastLogin(Mockito.<java.sql.Date>any());
        doNothing().when(userEntity).setModificationDate(Mockito.<java.sql.Date>any());
        doNothing().when(userEntity).setName(Mockito.<String>any());
        doNothing().when(userEntity).setPassword(Mockito.<String>any());
        doNothing().when(userEntity).setPhones(Mockito.<List<PhoneEntity>>any());
        doNothing().when(userEntity).setToken(Mockito.<String>any());
        userEntity.setActive(true);
        userEntity.setCreationDate(mock(java.sql.Date.class));
        userEntity.setEmail("jane.doe@example.org");
        userEntity.setIdUser(UUID.randomUUID());
        userEntity.setLastLogin(mock(java.sql.Date.class));
        userEntity.setModificationDate(mock(java.sql.Date.class));
        userEntity.setName("Name");
        userEntity.setPassword("iloveyou");
        userEntity.setPhones(new ArrayList<>());
        userEntity.setToken("ABC123");
        when(userRepository.save(Mockito.<UserEntity>any())).thenReturn(userEntity);

        User user = new User();
        user.setActive(true);
        user.setCreationDate(
                java.util.Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        user.setEmail("jane.doe@example.org");
        user.setId(UUID.randomUUID());
        user.setLastLogin(java.util.Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        user.setModificationDate(
                java.util.Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        user.setName("Name");
        user.setPassword("iloveyou");
        user.setPhones(new ArrayList<>());
        user.setToken("ABC123");

        // Act
        Optional<User> actualInsertUserResult = userPersistenceAdapter.insertUser(user);

        // Assert
        verify(userEntity).getActive();
        verify(userEntity).getCreationDate();
        verify(userEntity).getEmail();
        verify(userEntity).getIdUser();
        verify(userEntity).getLastLogin();
        verify(userEntity).getModificationDate();
        verify(userEntity).getName();
        verify(userEntity).getPassword();
        verify(userEntity).getPhones();
        verify(userEntity).getToken();
        verify(userEntity).setActive(Mockito.<Boolean>any());
        verify(userEntity).setCreationDate(Mockito.<java.sql.Date>any());
        verify(userEntity).setEmail(eq("jane.doe@example.org"));
        verify(userEntity).setIdUser(Mockito.<UUID>any());
        verify(userEntity).setLastLogin(Mockito.<java.sql.Date>any());
        verify(userEntity).setModificationDate(Mockito.<java.sql.Date>any());
        verify(userEntity).setName(eq("Name"));
        verify(userEntity).setPassword(eq("iloveyou"));
        verify(userEntity).setPhones(Mockito.<List<PhoneEntity>>any());
        verify(userEntity).setToken(eq("ABC123"));
        verify(userRepository).save(Mockito.<UserEntity>any());
        User getResult = actualInsertUserResult.get();
        assertEquals("ABC123", getResult.getToken());
        assertEquals("Name", getResult.getName());
        assertEquals("iloveyou", getResult.getPassword());
        assertEquals("jane.doe@example.org", getResult.getEmail());
        assertTrue(getResult.getActive());
        assertTrue(actualInsertUserResult.isPresent());
        assertEquals(phoneEntityList, getResult.getPhones());
    }

    /**
     * Method under test: {@link UserPersistenceAdapter#insertUser(User)}
     */
    @Test
    void testInsertUser4() {
        // Arrange
        UserEntity userEntity = mock(UserEntity.class);
        when(userEntity.getActive()).thenReturn(true);
        when(userEntity.getEmail()).thenReturn("jane.doe@example.org");
        when(userEntity.getName()).thenReturn("Name");
        when(userEntity.getPassword()).thenReturn("iloveyou");
        when(userEntity.getToken()).thenReturn("ABC123");
        when(userEntity.getCreationDate()).thenReturn(mock(java.sql.Date.class));
        when(userEntity.getLastLogin()).thenReturn(mock(java.sql.Date.class));
        when(userEntity.getModificationDate()).thenReturn(mock(java.sql.Date.class));
        ArrayList<PhoneEntity> phoneEntityList = new ArrayList<>();
        when(userEntity.getPhones()).thenReturn(phoneEntityList);
        UUID randomUUIDResult = UUID.randomUUID();
        when(userEntity.getIdUser()).thenReturn(randomUUIDResult);
        doNothing().when(userEntity).setActive(Mockito.<Boolean>any());
        doNothing().when(userEntity).setCreationDate(Mockito.<java.sql.Date>any());
        doNothing().when(userEntity).setEmail(Mockito.<String>any());
        doNothing().when(userEntity).setIdUser(Mockito.<UUID>any());
        doNothing().when(userEntity).setLastLogin(Mockito.<java.sql.Date>any());
        doNothing().when(userEntity).setModificationDate(Mockito.<java.sql.Date>any());
        doNothing().when(userEntity).setName(Mockito.<String>any());
        doNothing().when(userEntity).setPassword(Mockito.<String>any());
        doNothing().when(userEntity).setPhones(Mockito.<List<PhoneEntity>>any());
        doNothing().when(userEntity).setToken(Mockito.<String>any());
        userEntity.setActive(true);
        userEntity.setCreationDate(mock(java.sql.Date.class));
        userEntity.setEmail("jane.doe@example.org");
        userEntity.setIdUser(UUID.randomUUID());
        userEntity.setLastLogin(mock(java.sql.Date.class));
        userEntity.setModificationDate(mock(java.sql.Date.class));
        userEntity.setName("Name");
        userEntity.setPassword("iloveyou");
        userEntity.setPhones(new ArrayList<>());
        userEntity.setToken("ABC123");
        when(userRepository.save(Mockito.<UserEntity>any())).thenReturn(userEntity);

        UserEntity user = new UserEntity();
        user.setActive(true);
        user.setCreationDate(mock(java.sql.Date.class));
        user.setEmail("jane.doe@example.org");
        user.setIdUser(UUID.randomUUID());
        user.setLastLogin(mock(java.sql.Date.class));
        user.setModificationDate(mock(java.sql.Date.class));
        user.setName("Name");
        user.setPassword("iloveyou");
        user.setPhones(new ArrayList<>());
        user.setToken("ABC123");

        Phone phone = new Phone();
        phone.setCityCode("Oxford");
        phone.setCountryCode("GB");
        phone.setId(UUID.randomUUID());
        phone.setNumber("42");
        phone.setUser(user);

        ArrayList<Phone> phones = new ArrayList<>();
        phones.add(phone);

        User user2 = new User();
        user2.setActive(true);
        user2.setCreationDate(
                java.util.Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        user2.setEmail("jane.doe@example.org");
        user2.setId(UUID.randomUUID());
        user2.setLastLogin(java.util.Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        user2.setModificationDate(
                java.util.Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        user2.setName("Name");
        user2.setPassword("iloveyou");
        user2.setPhones(phones);
        user2.setToken("ABC123");

        // Act
        Optional<User> actualInsertUserResult = userPersistenceAdapter.insertUser(user2);

        // Assert
        verify(userEntity).getActive();
        verify(userEntity).getCreationDate();
        verify(userEntity).getEmail();
        verify(userEntity).getIdUser();
        verify(userEntity).getLastLogin();
        verify(userEntity).getModificationDate();
        verify(userEntity).getName();
        verify(userEntity).getPassword();
        verify(userEntity).getPhones();
        verify(userEntity).getToken();
        verify(userEntity).setActive(Mockito.<Boolean>any());
        verify(userEntity).setCreationDate(Mockito.<java.sql.Date>any());
        verify(userEntity).setEmail(eq("jane.doe@example.org"));
        verify(userEntity).setIdUser(Mockito.<UUID>any());
        verify(userEntity).setLastLogin(Mockito.<java.sql.Date>any());
        verify(userEntity).setModificationDate(Mockito.<java.sql.Date>any());
        verify(userEntity).setName(eq("Name"));
        verify(userEntity).setPassword(eq("iloveyou"));
        verify(userEntity).setPhones(Mockito.<List<PhoneEntity>>any());
        verify(userEntity).setToken(eq("ABC123"));
        verify(userRepository).save(Mockito.<UserEntity>any());
        User getResult = actualInsertUserResult.get();
        assertEquals("ABC123", getResult.getToken());
        assertEquals("Name", getResult.getName());
        assertEquals("iloveyou", getResult.getPassword());
        assertEquals("jane.doe@example.org", getResult.getEmail());
        assertTrue(getResult.getActive());
        assertTrue(actualInsertUserResult.isPresent());
        assertEquals(phoneEntityList, getResult.getPhones());
        assertSame(randomUUIDResult, getResult.getId());
    }

    /**
     * Method under test: {@link UserPersistenceAdapter#insertUser(User)}
     */
    @Test
    void testInsertUser5() {
        // Arrange
        UserEntity userEntity = mock(UserEntity.class);
        when(userEntity.getActive()).thenReturn(true);
        when(userEntity.getEmail()).thenReturn("jane.doe@example.org");
        when(userEntity.getName()).thenReturn("Name");
        when(userEntity.getPassword()).thenReturn("iloveyou");
        when(userEntity.getToken()).thenReturn("ABC123");
        when(userEntity.getCreationDate()).thenReturn(mock(java.sql.Date.class));
        when(userEntity.getLastLogin()).thenReturn(mock(java.sql.Date.class));
        when(userEntity.getModificationDate()).thenReturn(mock(java.sql.Date.class));
        ArrayList<PhoneEntity> phoneEntityList = new ArrayList<>();
        when(userEntity.getPhones()).thenReturn(phoneEntityList);
        UUID randomUUIDResult = UUID.randomUUID();
        when(userEntity.getIdUser()).thenReturn(randomUUIDResult);
        doNothing().when(userEntity).setActive(Mockito.<Boolean>any());
        doNothing().when(userEntity).setCreationDate(Mockito.<java.sql.Date>any());
        doNothing().when(userEntity).setEmail(Mockito.<String>any());
        doNothing().when(userEntity).setIdUser(Mockito.<UUID>any());
        doNothing().when(userEntity).setLastLogin(Mockito.<java.sql.Date>any());
        doNothing().when(userEntity).setModificationDate(Mockito.<java.sql.Date>any());
        doNothing().when(userEntity).setName(Mockito.<String>any());
        doNothing().when(userEntity).setPassword(Mockito.<String>any());
        doNothing().when(userEntity).setPhones(Mockito.<List<PhoneEntity>>any());
        doNothing().when(userEntity).setToken(Mockito.<String>any());
        userEntity.setActive(true);
        userEntity.setCreationDate(mock(java.sql.Date.class));
        userEntity.setEmail("jane.doe@example.org");
        userEntity.setIdUser(UUID.randomUUID());
        userEntity.setLastLogin(mock(java.sql.Date.class));
        userEntity.setModificationDate(mock(java.sql.Date.class));
        userEntity.setName("Name");
        userEntity.setPassword("iloveyou");
        userEntity.setPhones(new ArrayList<>());
        userEntity.setToken("ABC123");
        when(userRepository.save(Mockito.<UserEntity>any())).thenReturn(userEntity);

        UserEntity user = new UserEntity();
        user.setActive(true);
        user.setCreationDate(mock(java.sql.Date.class));
        user.setEmail("jane.doe@example.org");
        user.setIdUser(UUID.randomUUID());
        user.setLastLogin(mock(java.sql.Date.class));
        user.setModificationDate(mock(java.sql.Date.class));
        user.setName("Name");
        user.setPassword("iloveyou");
        user.setPhones(new ArrayList<>());
        user.setToken("ABC123");

        Phone phone = new Phone();
        phone.setCityCode("Oxford");
        phone.setCountryCode("GB");
        phone.setId(UUID.randomUUID());
        phone.setNumber("42");
        phone.setUser(user);

        UserEntity user2 = new UserEntity();
        user2.setActive(false);
        user2.setCreationDate(mock(java.sql.Date.class));
        user2.setEmail("john.smith@example.org");
        user2.setIdUser(UUID.randomUUID());
        user2.setLastLogin(mock(java.sql.Date.class));
        user2.setModificationDate(mock(java.sql.Date.class));
        user2.setName("42");
        user2.setPassword("Password");
        user2.setPhones(new ArrayList<>());
        user2.setToken("Token");

        Phone phone2 = new Phone();
        phone2.setCityCode("London");
        phone2.setCountryCode("GBR");
        phone2.setId(UUID.randomUUID());
        phone2.setNumber("Number");
        phone2.setUser(user2);

        ArrayList<Phone> phones = new ArrayList<>();
        phones.add(phone2);
        phones.add(phone);

        User user3 = new User();
        user3.setActive(true);
        user3.setCreationDate(
                java.util.Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        user3.setEmail("jane.doe@example.org");
        user3.setId(UUID.randomUUID());
        user3.setLastLogin(java.util.Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        user3.setModificationDate(
                java.util.Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        user3.setName("Name");
        user3.setPassword("iloveyou");
        user3.setPhones(phones);
        user3.setToken("ABC123");

        // Act
        Optional<User> actualInsertUserResult = userPersistenceAdapter.insertUser(user3);

        // Assert
        verify(userEntity).getActive();
        verify(userEntity).getCreationDate();
        verify(userEntity).getEmail();
        verify(userEntity).getIdUser();
        verify(userEntity).getLastLogin();
        verify(userEntity).getModificationDate();
        verify(userEntity).getName();
        verify(userEntity).getPassword();
        verify(userEntity).getPhones();
        verify(userEntity).getToken();
        verify(userEntity).setActive(Mockito.<Boolean>any());
        verify(userEntity).setCreationDate(Mockito.<java.sql.Date>any());
        verify(userEntity).setEmail(eq("jane.doe@example.org"));
        verify(userEntity).setIdUser(Mockito.<UUID>any());
        verify(userEntity).setLastLogin(Mockito.<java.sql.Date>any());
        verify(userEntity).setModificationDate(Mockito.<java.sql.Date>any());
        verify(userEntity).setName(eq("Name"));
        verify(userEntity).setPassword(eq("iloveyou"));
        verify(userEntity).setPhones(Mockito.<List<PhoneEntity>>any());
        verify(userEntity).setToken(eq("ABC123"));
        verify(userRepository).save(Mockito.<UserEntity>any());
        User getResult = actualInsertUserResult.get();
        assertEquals("ABC123", getResult.getToken());
        assertEquals("Name", getResult.getName());
        assertEquals("iloveyou", getResult.getPassword());
        assertEquals("jane.doe@example.org", getResult.getEmail());
        assertTrue(getResult.getActive());
        assertTrue(actualInsertUserResult.isPresent());
        assertEquals(phoneEntityList, getResult.getPhones());
        assertSame(randomUUIDResult, getResult.getId());
    }

    /**
     * Method under test: {@link UserPersistenceAdapter#insertUser(User)}
     */
    @Test
    void testInsertUser6() {
        // Arrange
        UserEntity user = new UserEntity();
        user.setActive(true);
        user.setCreationDate(mock(java.sql.Date.class));
        user.setEmail("jane.doe@example.org");
        user.setIdUser(UUID.randomUUID());
        user.setLastLogin(mock(java.sql.Date.class));
        user.setModificationDate(mock(java.sql.Date.class));
        user.setName("Name");
        user.setPassword("iloveyou");
        user.setPhones(new ArrayList<>());
        user.setToken("ABC123");

        PhoneEntity phoneEntity = new PhoneEntity();
        phoneEntity.setCityCode("Oxford");
        phoneEntity.setCountryCode("GB");
        UUID idPhone = UUID.randomUUID();
        phoneEntity.setIdPhone(idPhone);
        phoneEntity.setNumber("42");
        phoneEntity.setUser(user);

        ArrayList<PhoneEntity> phoneEntityList = new ArrayList<>();
        phoneEntityList.add(phoneEntity);
        UserEntity userEntity = mock(UserEntity.class);
        when(userEntity.getActive()).thenReturn(true);
        when(userEntity.getEmail()).thenReturn("42");
        when(userEntity.getName()).thenReturn("Name");
        when(userEntity.getPassword()).thenReturn("iloveyou");
        when(userEntity.getToken()).thenReturn("ABC123");
        when(userEntity.getCreationDate()).thenReturn(mock(java.sql.Date.class));
        when(userEntity.getLastLogin()).thenReturn(mock(java.sql.Date.class));
        when(userEntity.getModificationDate()).thenReturn(mock(java.sql.Date.class));
        when(userEntity.getPhones()).thenReturn(phoneEntityList);
        UUID randomUUIDResult = UUID.randomUUID();
        when(userEntity.getIdUser()).thenReturn(randomUUIDResult);
        doNothing().when(userEntity).setActive(Mockito.<Boolean>any());
        doNothing().when(userEntity).setCreationDate(Mockito.<java.sql.Date>any());
        doNothing().when(userEntity).setEmail(Mockito.<String>any());
        doNothing().when(userEntity).setIdUser(Mockito.<UUID>any());
        doNothing().when(userEntity).setLastLogin(Mockito.<java.sql.Date>any());
        doNothing().when(userEntity).setModificationDate(Mockito.<java.sql.Date>any());
        doNothing().when(userEntity).setName(Mockito.<String>any());
        doNothing().when(userEntity).setPassword(Mockito.<String>any());
        doNothing().when(userEntity).setPhones(Mockito.<List<PhoneEntity>>any());
        doNothing().when(userEntity).setToken(Mockito.<String>any());
        userEntity.setActive(true);
        userEntity.setCreationDate(mock(java.sql.Date.class));
        userEntity.setEmail("jane.doe@example.org");
        userEntity.setIdUser(UUID.randomUUID());
        userEntity.setLastLogin(mock(java.sql.Date.class));
        userEntity.setModificationDate(mock(java.sql.Date.class));
        userEntity.setName("Name");
        userEntity.setPassword("iloveyou");
        userEntity.setPhones(new ArrayList<>());
        userEntity.setToken("ABC123");
        when(userRepository.save(Mockito.<UserEntity>any())).thenReturn(userEntity);

        User user2 = new User();
        user2.setActive(true);
        user2.setCreationDate(
                java.util.Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        user2.setEmail("jane.doe@example.org");
        user2.setId(UUID.randomUUID());
        user2.setLastLogin(java.util.Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        user2.setModificationDate(
                java.util.Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        user2.setName("Name");
        user2.setPassword("iloveyou");
        user2.setPhones(new ArrayList<>());
        user2.setToken("ABC123");

        // Act
        Optional<User> actualInsertUserResult = userPersistenceAdapter.insertUser(user2);

        // Assert
        verify(userEntity).getActive();
        verify(userEntity).getCreationDate();
        verify(userEntity).getEmail();
        verify(userEntity).getIdUser();
        verify(userEntity).getLastLogin();
        verify(userEntity).getModificationDate();
        verify(userEntity).getName();
        verify(userEntity).getPassword();
        verify(userEntity).getPhones();
        verify(userEntity).getToken();
        verify(userEntity).setActive(Mockito.<Boolean>any());
        verify(userEntity).setCreationDate(Mockito.<java.sql.Date>any());
        verify(userEntity).setEmail(eq("jane.doe@example.org"));
        verify(userEntity).setIdUser(Mockito.<UUID>any());
        verify(userEntity).setLastLogin(Mockito.<java.sql.Date>any());
        verify(userEntity).setModificationDate(Mockito.<java.sql.Date>any());
        verify(userEntity).setName(eq("Name"));
        verify(userEntity).setPassword(eq("iloveyou"));
        verify(userEntity).setPhones(Mockito.<List<PhoneEntity>>any());
        verify(userEntity).setToken(eq("ABC123"));
        verify(userRepository).save(Mockito.<UserEntity>any());
        User getResult = actualInsertUserResult.get();
        List<Phone> phones = getResult.getPhones();
        assertEquals(1, phones.size());
        Phone getResult2 = phones.get(0);
        assertEquals("42", getResult2.getNumber());
        assertEquals("42", getResult.getEmail());
        assertEquals("ABC123", getResult.getToken());
        assertEquals("GB", getResult2.getCountryCode());
        assertEquals("Name", getResult.getName());
        assertEquals("Oxford", getResult2.getCityCode());
        assertEquals("iloveyou", getResult.getPassword());
        assertTrue(getResult.getActive());
        assertTrue(actualInsertUserResult.isPresent());
        assertSame(user, getResult2.getUser());
        assertSame(idPhone, getResult2.getId());
        assertSame(randomUUIDResult, getResult.getId());
    }
}
