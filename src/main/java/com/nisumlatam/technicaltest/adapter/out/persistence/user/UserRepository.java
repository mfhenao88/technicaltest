package com.nisumlatam.technicaltest.adapter.out.persistence.user;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends CrudRepository<UserEntity, Long> {
    UserEntity findByEmail(@Param("email") String email);
}
