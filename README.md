# Spring Boot Project Technical test Nisum latam

This project is a Spring Boot application that provides APIs for user management.

# Hexagonal Architecture

This project implement the hexagonal architecture pattern, also known as Ports and Adapters architecture. The hexagonal architecture promotes a modular and flexible design, with a clear separation between the core domain logic and the external systems.

## Hexagonal Architecture Overview

The hexagonal architecture is structured around three main components:

- **Core Domain Logic**: This component contains the business rules, entities, and domain services that encapsulate the unique functionality and value of the application. It represents the heart of the system and is independent of external concerns.

- **Ports**: Ports are the interfaces through which the application interacts with the external world. These interfaces define the operations that the application provides or expects to receive. Examples of ports include REST APIs, messaging systems, databases, and external services.

- **Adapters**: Adapters are the implementations of the ports. They are responsible for translating the requests and responses between the core domain logic and the external systems. Adapters encapsulate the details of interacting with external systems, such as data serialization, network communication, and database access.

## Implementation in This Project

In this project, the hexagonal architecture is implemented to ensure a modular and testable design. The core domain logic is separated from the infrastructure concerns, allowing for easier maintenance, testing, and evolution of the software system.

![Hexagonal Architecture](https://alistair.cockburn.us/wp-content/uploads/2018/02/Hexagonal-architecture-basic-1.gif)

The image above illustrates the hexagonal architecture of the project, showing the core domain logic at the center, surrounded by ports and adapters representing the interfaces and implementations for interacting with external systems.


## Running the Application

To run the application, follow these steps:

1. Ensure you have Java installed on your machine.
2. Clone the project repository.
3. Navigate to the project directory.
4. Run the following Class: **TechnicaltestApplication**


This class will build and run the application.

## Swagger Documentation

The Swagger documentation for the APIs can be accessed at: `http://localhost:8009/technicaltest/swagger-ui/index.html`

This documentation provides information about the available endpoints and how to interact with them.

## Example: Inserting a User

To insert a user into the database, send a POST request to the `http://localhost:8009/technicaltest/user` endpoint with the following JSON body:

```json
{
   "name": "Juan Rodriguez",
   "email": "juan@rodriguez.org",
   "password": "hunter23",
   "phones": [
      {
         "number": "1234567",
         "citycode": "1",
         "countrycode": "57"
      }
   ]
}
```
If the user is successfully inserted, the API will respond with the following data:

```json
{
  "idUser": "db8e703b-b4ca-4218-b488-c1711a8e0218",
  "creationDate": "2024-03-10",
  "modificationDate": "2024-03-10",
  "lastLogin": "2024-03-10",
  "token": "4c2746ae-2e50-4c71-8f7f-bfadd79e956d",
  "active": true
}
```

If the email already exists in the database, the API will respond an 400with an error message in the following format:
```json
{
  "mensaje": "Email is already registered"
}
```

