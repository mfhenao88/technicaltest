package com.nisumlatam.technicaltest.adapter.in.web.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.nisumlatam.technicaltest.common.validator.Password;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;


import java.util.List;

@ApiModel(description = "Request DTO for User")
public class UserRequest {
    @ApiModelProperty(value = "User name")
    @NotBlank(message = "Name cannot be blank")
    @JsonProperty("name")
    private String name;

    @ApiModelProperty(value = "User email")
    @NotBlank(message = "Email cannot be blank")
    @Pattern(regexp = "[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,5}", message = "Invalid email")
    @JsonProperty("email")
    private String email;

    @ApiModelProperty(value = "User password")
    @NotBlank(message = "Password cannot be blank")
    @Password(message = "Invalidate password")
    @JsonProperty("password")
    private String password;

    @ApiModelProperty(value = "User phones")
    private List<PhoneRequest> phones;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<PhoneRequest> getPhones() {
        return phones;
    }

    public void setPhones(List<PhoneRequest> phones) {
        this.phones = phones;
    }
}
