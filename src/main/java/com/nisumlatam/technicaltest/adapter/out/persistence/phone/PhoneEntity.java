package com.nisumlatam.technicaltest.adapter.out.persistence.phone;

import com.nisumlatam.technicaltest.adapter.out.persistence.user.UserEntity;
import jakarta.persistence.*;

import java.util.UUID;


@Entity
@Table(name = "phone")
public class PhoneEntity {
    @Id
    @Column(name = "phone_id")
    private UUID idPhone;
    @Column(name = "number")
    private String number;
    @Column(name = "city_code")
    private String cityCode;
    @Column(name = "country_code")
    private String countryCode;
    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private UserEntity user;

    public UUID getIdPhone() {
        return idPhone;
    }

    public void setIdPhone(UUID idPhone) {
        this.idPhone = idPhone;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }
}
