package com.nisumlatam.technicaltest.common.exceptionhandler;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Error")
public class ErrorApi {

    @ApiModelProperty(value = "mensaje")
    @JsonProperty("mensaje")
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
