package com.nisumlatam.technicaltest.adapter.in.web;

import com.nisumlatam.technicaltest.adapter.in.web.model.UserRequest;
import com.nisumlatam.technicaltest.adapter.in.web.model.UserResponse;
import com.nisumlatam.technicaltest.application.port.in.RegisterUserPort;
import com.nisumlatam.technicaltest.application.port.in.UserCommand;
import com.nisumlatam.technicaltest.common.component.WebAdapter;
import com.nisumlatam.technicaltest.common.exceptionhandler.ErrorApi;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@WebAdapter
@RestController
@RequestMapping("/user")
@Validated
public class UserController {

    private RegisterUserPort registerUserPort;
    private ModelMapper modelMapper;

    public UserController(RegisterUserPort registerUserPort) {
        this.registerUserPort = registerUserPort;
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setSkipNullEnabled(true);
        this.modelMapper = modelMapper;
    }

    @Operation(
            summary = "Insert user data",
            description = "Add the user to the database if it does not already exist.",
            tags = {"post"})
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = {@Content(schema = @Schema(implementation = UserResponse.class), mediaType = "application/json")}),
            @ApiResponse(responseCode = "409", content = {@Content(schema = @Schema(implementation = ErrorApi.class), mediaType = "application/json")}),
            @ApiResponse(responseCode = "500", content = {@Content(schema = @Schema(implementation = ErrorApi.class), mediaType = "application/json")})})
    @PostMapping
    public ResponseEntity<UserResponse> registerUser(@Valid @RequestBody UserRequest userRequest) {
        return ResponseEntity.ok(modelMapper.map(registerUserPort.registerUser(modelMapper.map(userRequest, UserCommand.class)), UserResponse.class));
    }

}
