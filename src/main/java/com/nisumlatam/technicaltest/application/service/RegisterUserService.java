package com.nisumlatam.technicaltest.application.service;

import com.nisumlatam.technicaltest.application.port.in.RegisterUserPort;
import com.nisumlatam.technicaltest.application.port.in.UserCommand;
import com.nisumlatam.technicaltest.application.port.out.InsertUserPort;
import com.nisumlatam.technicaltest.application.port.out.LoadUserPort;
import com.nisumlatam.technicaltest.common.component.UseCase;
import com.nisumlatam.technicaltest.common.exceptionhandler.ValidationException;
import com.nisumlatam.technicaltest.domain.User;
import jakarta.transaction.Transactional;
import org.modelmapper.ModelMapper;

import java.util.Date;
import java.util.UUID;

@UseCase
public class RegisterUserService implements RegisterUserPort {

    private LoadUserPort loadUserPort;
    private InsertUserPort insertUserPort;
    private ModelMapper modelMapper;

    public RegisterUserService(LoadUserPort loadUserPort, InsertUserPort insertUserPort) {
        this.loadUserPort = loadUserPort;
        this.insertUserPort = insertUserPort;
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setSkipNullEnabled(true);
        this.modelMapper = modelMapper;
    }

    @Transactional
    public UserCommand registerUser(UserCommand userCommand) {
        if (loadUserPort.load(userCommand.getEmail()).isPresent()) {
            throw new ValidationException("Email is ready registered");
        } else {
            User user = modelMapper.map(userCommand, User.class);
            user.setActive(true);
            user.setLastLogin(new Date());
            user.setCreationDate(new Date());
            user.setModificationDate(new Date());
            user.setToken(UUID.randomUUID().toString());
            return insertUserPort.insertUser(user)
                    .map(userReturn -> modelMapper.map(userReturn, UserCommand.class))
                    .orElseThrow(() -> new RuntimeException("Error executing user insert"));
        }
    }
}
