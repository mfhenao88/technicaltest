package com.nisumlatam.technicaltest.application.port.in;

import jakarta.validation.constraints.NotBlank;


public class PhoneCommand {
    @NotBlank(message = "Number cannot be blank")
    private String number;
    @NotBlank(message = "City code cannot be blank")
    private int cityCode;
    @NotBlank(message = "Country code cannot be blank")
    private int countryCode;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getCityCode() {
        return cityCode;
    }

    public void setCityCode(int cityCode) {
        this.cityCode = cityCode;
    }

    public int getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(int countryCode) {
        this.countryCode = countryCode;
    }
}
